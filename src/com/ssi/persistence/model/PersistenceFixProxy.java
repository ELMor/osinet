package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;

public class PersistenceFixProxy implements ProxyInterface
{
	//Inst Vars
	String oid;
	PersistenceAccessor accessor;

	//Class Methods
	public PersistenceFixProxy(String anOID, PersistenceAccessor anAccessor)
	{
		oid = anOID;
		accessor = anAccessor;
	}

	private PersistenceAccessor accessor()
	{
		return accessor;
	}

	public Class classOfPrefix()
		throws ClassNotFoundException
	{
		return this.accessor().persistentClass();
	}

	public Class classRepresented()
		throws ClassNotFoundException
	{
		return Class.forName("com.ssi.persistence.model.ProxyInterface");
	}

	
	//Inst Methods
	public void fetchRealFor(PersistentObjectInterface anOwner,String aMessage)
		throws IllegalAccessException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   ClassNotFoundException,
			   InstantiationException,
			   SQLException,
			   Exception
	{
		 this.getMethodFor(anOwner, aMessage).invoke(anOwner, new ProxyInterface[] {((ProxyInterface)this.getRealObject())});
	}

	private Method getMethodFor(PersistentObjectInterface anOwner,String aMessage)
			throws NoSuchMethodException,
				   ClassNotFoundException
		{
			Class aClass = this.classRepresented();
			return anOwner.getClass().getMethod(aMessage, new Class[] {aClass});
		}

	private PersistentObjectInterface getRealObject()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException,
			   Exception
	{
		return this.accessor().instanceOfOID(this.oid());
	}

	//
	// Testing
	//

	public boolean isProxy()
	{
		return true;
	}

	public String oid()
	{
		return oid;
	}

	public void oid(String anOID)
	{
		oid = anOID;
	}

	private PersistenceManager persistenceManager()
	{
		return this.accessor().persistenceManager();
	}
}