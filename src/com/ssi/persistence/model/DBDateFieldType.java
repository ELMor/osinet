package com.ssi.persistence.model;

import java.util.GregorianCalendar;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public class DBDateFieldType extends DBFieldType
{
	public String fieldClassName()
	{
		return "java.util.GregorianCalendar";
	}
public static boolean represents(Field aField)
{
	try
	{
		return aField.getType() == Class.forName("java.util.GregorianCalendar");
	}
	catch (ClassNotFoundException ex)
	{
		return false;
	}
}
	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		aSetter.invoke(newInstance, new GregorianCalendar[] {aField.getGregorianCalendar()});
	}
}