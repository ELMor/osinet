package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;

public class JDBCField extends com.ssi.persistence.model.DBField
{
	ResultSet resultSet;
	String columnName;
	int columnIndex;
	Object answer = null;
/**
 * getString method comment.
 */
public JDBCField(ResultSet aResultSet, int aColumnIndex)
{
	resultSet = aResultSet;
	columnIndex = aColumnIndex;
}
/**
 * getString method comment.
 */
public JDBCField(ResultSet aResultSet, String aColumnName)
{
	resultSet = aResultSet;
	columnName = aColumnName;
}
/**
 * This method was created in VisualAge.
 */
public int columnIndex() {
	return columnIndex;
}
/**
 * This method was created in VisualAge.
 */
public String columnName() {
	return columnName;
}
/**
 * getBoolean method comment.
 */
public Boolean getBoolean() throws SQLException
{
	if ( this.isLoaded() ) return (Boolean)answer;
	if (columnName == null)
	{
		answer = new Boolean(resultSet.getBoolean(columnIndex));
	}
	else
	{
		answer = new Boolean(resultSet.getBoolean(columnName));
	}
	return (Boolean)answer;
}

public Date getDate() throws SQLException
{
	if ( this.isLoaded() ) return (Date) answer;
	if (columnName == null)
	{
		answer = resultSet.getDate(columnIndex);
	}
	else
	{
		answer = resultSet.getDate(columnName);
	}
	return (Date)answer;
}


public GregorianCalendar getGregorianCalendar() throws SQLException
{
	if ( this.isLoaded() ) return (GregorianCalendar) answer;
	Date aDate;
	if (columnName == null){
		aDate = resultSet.getDate(columnIndex);
	}
	else {
		aDate = resultSet.getDate(columnName);
	}
	GregorianCalendar answer;
	if(aDate != null) {
		answer = new GregorianCalendar(aDate.getDate(), aDate.getMonth(), aDate.getYear()+1900);
		answer.setTime(aDate);
	}
	else {
		answer = null;
	}
	return answer;
}



public Timestamp getTimestamp() throws SQLException
{
	if ( this.isLoaded() ) return (Timestamp) answer;
	if (columnName == null)
	{
		answer = resultSet.getTimestamp(columnIndex);
	}
	else
	{
		answer = resultSet.getTimestamp(columnName);
	}
	return (Timestamp)answer;
}


/**
 * getString method comment.
 */
public Character getCharacter() throws SQLException
{
	if ( this.isLoaded() ) return (Character) answer;

	String aString = "";

	if (columnName == null)
	{
		aString = resultSet.getString(columnIndex);
	}
	else
	{
		aString = resultSet.getString(columnName);
	}

	if (aString == null) answer = null;
	else answer = new Character(aString.trim().toCharArray()[0]);

	return (Character)answer;
}
/**
 * This method was created in VisualAge.
 */
public Double getDouble() throws SQLException
{
	if ( this.isLoaded() ) return (Double) answer;
	if (columnName == null)
	{
		answer = new Double(resultSet.getDouble(columnIndex));
	}
	else
	{
		answer = new Double(resultSet.getDouble(columnName));
	}
	return (Double)answer;
}
/**
 * This method was created in VisualAge.
 */
public Float getFloat() throws SQLException
{
	if ( this.isLoaded() ) return (Float) answer;
	if (columnName == null)
	{
		answer = new Float(resultSet.getFloat(columnIndex));
	}
	else
	{
		answer = new Float(resultSet.getFloat(columnName));
	}
	return (Float)answer;
}
/**
 * getInteger method comment.
 */
public Integer getInteger() throws SQLException
{
	if ( this.isLoaded() ) {
		if (answer.getClass().getName().equals("java.lang.Integer") )
			return (Integer)answer;
		else
			return new Integer((String)answer);
	}

	if (columnName != null)
	{
		answer = new Integer(resultSet.getInt(columnName));
	}
	else
	{
		answer = new Integer(resultSet.getInt(columnIndex));
	}
	return (Integer)answer;
}
/**
 * This method was created in VisualAge.
 */
public Long getLong() throws SQLException
{
	if ( this.isLoaded() ) return (Long) answer;
	if (columnName == null)
	{
		answer = new Long(resultSet.getLong(columnIndex));
	}
	else
	{
		answer = new Long(resultSet.getLong(columnName));
	}
	return (Long)answer;
}
/**
 * getString method comment.
 */
public String getString() throws SQLException
{
    
    String result = null;
	if ( this.isLoaded() ) {
	    result = (String) answer;
	    return result;
	}
	if (columnName == null){
		answer = resultSet.getString(columnIndex);
	}
	else{
		answer = resultSet.getString(columnName);
	}
	
	result = (String) answer;
	return result;
}

/**
 * This method was created in VisualAge.
 */
protected boolean isLoaded(){
	return answer != null;
}
}