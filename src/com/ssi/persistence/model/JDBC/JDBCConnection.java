package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */
import java.sql.Connection;
import java.sql.SQLException;
import com.ssi.persistence.model.*;

public class JDBCConnection extends com.ssi.persistence.model.DBConnection
{
	private Connection connection;
/**
 * connection method comment.
 */
public JDBCConnection(Connection aConnection, ConnectionPool aPool)
{
	connection = aConnection;
	this.setConnectionPool(aPool);
}
/**
 * close method comment.
 */
public void close() throws SQLException
{
	((Connection)this.connection()).close();
	super.close();
}
/**
 * connection method comment.
 */
public Object connection() {
	return connection;
}
/**
 * isConnected method comment.
 */
public boolean isConnected()
{
	try
	{
		return !((Connection)this.connection()).isClosed();
	}
	catch (java.sql.SQLException anException)
	{
		return false;
	}
}
}