package com.ssi.persistence.model.JDBC;

/**
 * This type was created in VisualAge.
 */

import com.ssi.persistence.model.DBConnection;
import com.ssi.persistence.model.DBResultSet;
import java.sql.*;

abstract public class JDBCCommand extends com.ssi.persistence.model.DBCommand
{
	static long statementCounter = 0;
	DBConnection connection;
	String commandString;
	Statement command;
	Boolean displayCounter = new Boolean(false);

public void displayCounter(Boolean aBoolean)
{
	displayCounter = aBoolean;
}

public Boolean displayCounter()
{
	return displayCounter;
}

public void closeCommand() throws SQLException {
	this.command().close();
	this.connection().returnConnection();
	if (this.displayCounter().booleanValue())
	{
	    statementCounter--;
	    this.outputStatementCounter();
	}
}

public void outputStatementCounter()
{
	System.out.println( "Statements:" + statementCounter );
}

public Statement command()
	throws SQLException
{
	if (command == null) {
		this.command(((Connection)connection.connection()).createStatement());
		if (this.displayCounter().booleanValue())
		{
	        statementCounter++;
	        this.outputStatementCounter();
		}
	}
	return command;
}
protected void command (Statement aStatement) {
	command = aStatement;
}

abstract public DBResultSet execute() throws SQLException;

static public JDBCCommand instanceFor(String aCommandString)
{
	JDBCCommand answer;
	if (JDBCAnswerCommand.canHandle(aCommandString))
	{
		answer = new JDBCAnswerCommand();
	}
	else
	{
		answer = new JDBCNoAnswerCommand();
	}
	return answer;
}

public void setActiveConnection(DBConnection aConnection)
{
	connection = aConnection;
}

public void setCommandString(String aCommandString)
{
	commandString = aCommandString;
}

public DBConnection connection()
{
	return this.connection;
}
}