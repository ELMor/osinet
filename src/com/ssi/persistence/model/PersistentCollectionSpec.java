package com.ssi.persistence.model;

import java.util.Vector;
import java.util.Enumeration;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PersistentCollectionSpec
{
	//Inst Vars
	String fieldName;
	String accessorName;
	String ownerClassName;
	String ownerAccessorName;
	Vector contentsClassNames;

	/**Represents the specification or mapping of a 1 to many
	object relationship. The 1 object is also know as the
	owner object since it is the owner of the collection
	of many.
	For a comment on the instance variables content see the constructor definition.*/

	public PersistentCollectionSpec(String aFieldName, String anAccessorName, String anOwnerClassName, String anOwnerAccessorName, Vector aVector)
	{
		fieldName = aFieldName;                 //Name of the field in the many which references the owner object in the database
		accessorName = anAccessorName;          //Name of the method in the many objects which sets the owner object
		ownerClassName = anOwnerClassName;      //Name of the class of the owner object
		contentsClassNames = aVector;           //Name of the classes of the objects contained by the receiver's collection object
		ownerAccessorName = anOwnerAccessorName;//Name of the method which answers the collection defined by the receiver in the owner object
	}



	public String accessorName()
	{
		return accessorName;
	}

	public Vector contentClasses()
		throws ClassNotFoundException
	{
		Vector answer = new Vector();
		Enumeration iterator = this.contentsClassNames().elements();
		while(iterator.hasMoreElements())
		{
			answer.addElement(Class.forName(((String)iterator.nextElement())));
		}
		return answer;
	}

	public Vector contentsClassNames()
	{
		return contentsClassNames;
	}

	public String fieldName()
	{
		return fieldName;
	}

	private Method getOwnerAccessorMethod()
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		return this.ownerClass().getMethod(this.ownerAccessorName(), null);
	}

	public String ownerAccessorName()
	{
		return ownerAccessorName;
	}

	public void ownerAccessorName(String anAccessorName)
	{
		ownerAccessorName = anAccessorName;
	}

	public Class ownerClass()
		throws ClassNotFoundException
	{
		return Class.forName(this.ownerClassName());
	}

	public String ownerClassName()
	{
		return ownerClassName;
	}

	protected PersistentCollection yourCollectionIn(PersistentObjectInterface anInstance)
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException
	{
	    //Answers the PersistentCollection object contained
	    //in anInstance which represents the receiver's definition
		return (PersistentCollection)this.getOwnerAccessorMethod().invoke(anInstance, null);
	}
}