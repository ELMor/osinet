package com.ssi.persistence.model.salud;

import com.sybase.jdbcx.SybDriver;
import oracle.jdbc.driver.OracleDriver;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.JDBC.*;
import com.ssi.model.salud.*;


public class SistemaSiapwinConfiguration extends SistemaSaludConfiguration{

	public String databaseName()
	{
		return "Siapwin_Dm6";
	}

	public String getConnectionUser()
	{
		return "Admin";
	}

	public String getConnectionPassword()
	{
		return "sql";
	}

	public void setAccessorsTo(PersistenceManager aManager)
	{
		//Sets the accesso for the persistent object to be used
		//by the application. Subclases must use super when
		//redefining this method.
		//Note that you could explicity specify a new cache for
		//each accessor. See com.ssi.persistence.cache package.

		aManager.addAccessor(PacienteSiapwinAccessor.getInstance(aManager));
		aManager.addAccessor(SexoSiapwinAccessor.getInstance(aManager));
		
		
		//AN
		aManager.addAccessor(EpisodioSiapwinAccessor.getInstance(aManager));
		aManager.addAccessor(EncuentroSiapwinAccessor.getInstance(aManager));
		aManager.addAccessor(ActividadSiapwinAccessor.getInstance(aManager));

	}


	/**
	 * Returns the number tries to make in order to connect to the DB.
	 */
	public int connectionAttempts()
	{
		return 10;
	}


	/**
	 * Returns the number of milliseconds to hold
	 * a requester of a connection if no connections
	 * are available
	 */
	public int poolWaitTime()
	{
		return 1000;
	}

	/**
	 * Retorna la cantidad de conecciones a utilizar dentro del pool.
	 */
	public int maxConnections()
	{
		return 3; //10;
	}

}