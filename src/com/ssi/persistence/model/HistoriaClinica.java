// Decompiled by JAD v1.5.5. Copyright 1997-98 Pavel Kouznetsov.
// JAD Home Page:      http://web.unicom.com.cy/~kpd/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   HistoriaClinica.java

package com.ssi.persistence.model;

import com.ssi.model.salud.SistemaNovahis;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            ReadWritePersistentObject, PersistenceManager, UserAccessor, User, 
//            PersistenceAccessor, DBField

public class HistoriaClinica extends ReadWritePersistentObject
{

	public static JDBCAnswerResultSet ejecutarConsulta(String idUsuario)
	{
		
		
		try
		{
			PersistenceManager manager = SistemaNovahis.getInstance().getPersistenceManager();
			return (JDBCAnswerResultSet)manager.executeQuery(getQueryString(idUsuario));
		}
		catch(Exception e)
		{
			LogFile.log("HistoriaClinica.ejecutarConsulta()" + e.toString());
			LogFile.log(e);
			return null;
		}
	}

	public String getId()
	{
		return id;
	}

	public static PersistenceAccessor getPersistenceAccessor()
	{
		return UserAccessor.getInstance();
	}

	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		return User.getPersistenceAccessor().persistenceManager();
	}

	public static String getQueryString(String idCliente)
	{
		String strQuery = "select nhc from HC where codigo_cliente='" + idCliente + "' and activa_sn=1";
		LogFile.log("Query de historia clinica: " + strQuery);
		return strQuery;
	}

	public static Vector instances()
		throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
	{
		return User.getPersistenceManager().instancesOf(Class.forName("com.ssi.persistence.model.User"));
	}

	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
		return User.getPersistenceManager();
	}

	public void setId(String newId)
	{
		id = newId;
	}

	public static HistoriaClinica userNamed(String idCliente)
		throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
	{
		HistoriaClinica hc = new HistoriaClinica();
		JDBCAnswerResultSet rs = ejecutarConsulta(idCliente);
		try
		{
			hc.setId(rs.getField(1).getString());
		}
		catch(Exception ex)
		{
			hc.setId("s/d");
		}
		finally{
			rs.close();
		}	
		
		return hc;
	}

	public HistoriaClinica()
	{
	}

	String id;
}