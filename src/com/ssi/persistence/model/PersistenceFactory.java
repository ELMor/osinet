package com.ssi.persistence.model;

import java.sql.SQLException;
import com.ssi.persistence.model.*;

public abstract class PersistenceFactory
{
	private PersistenceConfiguration persistenceConfiguration;

	
	public PersistenceFactory(PersistenceConfiguration aConfiguration)
	{
		persistenceConfiguration = aConfiguration;
	}
	
	public void setDBEngineType(PersistenceConfiguration aConfiguration)
	{
		this.persistenceConfiguration = aConfiguration;
	}

	
	public PersistenceConfiguration getPersistenceConfiguration()
	{
		return this.persistenceConfiguration;
	}
	
	/**
	 * Answers an appropriate connection String
	 * for the receiverīs connection type
	 */
	public abstract String connectionStringFor(String aDatabaseName, String aUserName, String aPassword);

	/**
	 * Answers an appropriate connection String
	 * for the receiverīs connection type
	 */
	public abstract String connectionStringFor(String aDatabaseName);

	/**
	 * Answers a new SQL command which represents aCommandString
	 */
	public abstract DBCommand dbCommand(String aCommandString);

	/**
	 * Answers a connection object not yet connected to the DB
	 */
	public abstract DBConnection dbConnection();

	/**
	 * Answers an already connected connection
	 */
	public abstract DBConnection dbConnection(String aConnectionString, ConnectionPool aPool)
		throws SQLException;

	/**
	 * Answers an already connected connection
	 */
	public abstract DBConnection dbConnection(String aConnectionString, String user, String password, ConnectionPool aPool)
		throws SQLException;
}