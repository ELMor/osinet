package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Vector;
import java.sql.SQLException;
//import com.ssi.persistence.ui.*;


public abstract class PersistentObject
	implements	PersistentObjectInterface,
				ProxyInterface
{
	
	/**
	 * WARNING: Should be redefined by subclasses!
	 */
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return PersistenceAccessor.getInstance();
	}

	
	public static PersistentObjectInterface instanceOfOID(String anOID)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException,
			   Exception
	{
		//Answers the instance of the receiver whose OID is anOID
		return PersistentObject.getPersistenceAccessor().instanceOfOID(anOID);
	}
	
	
	/**
	 * This method was created in VisualAge.
	 * @return boolean
	 * @param aClass java.lang.Class
	 */
	public static boolean isClassForeignPersistent (Class aClass) throws ClassNotFoundException {
		/*Answers wheter aClass is mapped to a read only table,
		usually on a foreign (from another system) table.*/

		if (aClass == null || aClass == Class.forName("java.lang.Object") ) return false;
		else return (aClass == Class.forName("com.ssi.persistence.model.ReadPersistentObject") || isClassForeignPersistent(aClass.getSuperclass()));

	}
	
	
	/**
	 * This method was created in VisualAge.
	 * @return boolean
	 * @param aClass java.lang.Class
	 */
	public static boolean isClassPersistent (Class aClass) throws ClassNotFoundException {

		if (aClass == null || aClass == Class.forName("java.lang.Object") ) return false;
		else return (aClass == Class.forName("com.ssi.persistence.model.PersistentObject") || isClassPersistent(aClass.getSuperclass()));

	}

	
	
	
	public Class classOfPrefix()
		throws ClassNotFoundException
	{
		return this.classRepresented();
	}
	public Class classRepresented()
	{
		return this.getClass();
	}

	public abstract void delete()
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   SQLException;

	public void fetchRealFor(PersistentObjectInterface anOwner,String aMessage)
		throws IllegalAccessException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   ClassNotFoundException,
			   InstantiationException
	{
		//Fetches the real object the receiver represents. Answers self.
	}

	public abstract boolean isPersistent();

	public abstract String oid();

	public abstract void oid(String anOID);

	public abstract void save()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				InstantiationException,
				SQLException;

	public boolean isProxy()
	{
		return false;
	}

	public String oidPrefix()
			throws ClassNotFoundException,
				   NoSuchMethodException
	{
		//answers the oid prefix for the receiverīs instances
		return this.persistenceManager().oidPrefixForObject(this);
	}


	/**
	 * WARNING: Should be redefined by subclasses!
	 */
	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return PersistentObject.getPersistenceAccessor().persistenceManager();
	}


	//Answers the persistence manager for the receiver
	//return PersistentObject.getPersistenceManager();
	public abstract PersistenceManager persistenceManager()
		throws ClassNotFoundException;


	public String tableName()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException
	{
		//Answers the name of the table where the receiver is kept
		return this.persistenceManager().tableNameFor(this);
	}
}