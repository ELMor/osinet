package com.ssi.persistence.model;

import java.util.*;
import java.lang.reflect.*;
import java.sql.SQLException;

public class PersistentCollection
{

	/*Represents a collection of persistent objects the collection
	can contain objects of different classes. Note that although an
	object can have mre than one persistent collection it can NOT
	put objects of the same class in both collection!*/


	// Inst Vars
	Vector collection;
	PersistentObjectInterface owner;



	//Class Methods
	public PersistentCollection(PersistentObjectInterface anOwner)
	{
		owner = anOwner;
	}


	// Wrapped protocol
	public void addElement(Object anObject)
		throws NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   ClassNotFoundException,
			   InstantiationException,
			   SQLException

	{
	    //Adds anObject to the collection.
	    //Note that you MUST tell anObject or the collection
	    //to save itself for the change to be reflected in the
	    //database!

		this.collection().addElement(anObject);
		Method aSetter = anObject.getClass().getMethod(this.ownerAccessorNameFor(((PersistentObject)this.owner())), new Class[] {Class.forName("com.ssi.persistence.model.ProxyInterface")});
		aSetter.invoke(anObject, new Object[] {this.owner()});
	}

	public final int capacity()
		throws Exception
	{
		return this.collection().capacity();
	}

	private Vector collection()
		throws InstantiationException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		if (collection == null)
		{
			this.setCollection();
		}
		return collection;
	}

	protected void collection(Vector aVector)
	{
		collection = aVector;
	}

	public final boolean contains(Object anElement)
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		this.fetchRealObjects();
		return this.collection().contains(anElement);
	}

	public final synchronized Object elementAt( int index )
		throws Exception
	{
		this.fetchRealObjects();
		return this.collection().elementAt(index);
	}

	public final synchronized Enumeration elements()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		this.fetchRealObjects();
		return this.collection().elements();
	}

	private void fetchRealObjects()
		throws InstantiationException,
			  IllegalAccessException,
			  ClassNotFoundException,
			  InvocationTargetException,
			  NoSuchMethodException,
			  SQLException
	{
		if (this.needsToFetchRealObjects())
		{
			this.persistenceManager().retrieveCollection(this);
		}
	}

	public final synchronized Object firstElement()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		this.fetchRealObjects();
		return this.collection().firstElement();
	}

	protected Dictionary getInstancesByClass()
		throws InstantiationException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		Dictionary answer = new Hashtable();
		Enumeration iterator = this.collection().elements();
		while (iterator.hasMoreElements())
		{
			ProxyInterface element = ((ProxyInterface)iterator.nextElement());
			Class aClass = element.classOfPrefix();
			if (answer.get(aClass) == null)
			{
				answer.put(aClass, new Vector());
			}
			((Vector)answer.get(aClass)).addElement(element);
		}
		return answer;
	}

	public final boolean isEmpty()
		throws Exception
	{
		return this.collection().isEmpty();
	}

	private boolean needsToFetchRealObjects()
		throws InstantiationException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		boolean needs = false;
		Enumeration iterator = this.collection().elements();
		while (iterator.hasMoreElements() & !needs)
		{
			if (((ProxyInterface)iterator.nextElement()).isProxy())
			{
				needs = true;
				return needs;
			}
		}
		return needs;
	}

	// Inst Methods
	protected PersistentObjectInterface owner()
	{
		return owner;
	}

	private String ownerAccessorNameFor(PersistentObject anObject)
		throws ClassNotFoundException,
			   NoSuchMethodException
	{
		PersistenceAccessor accessor = this.persistenceManager().accessorForClass(anObject.getClass());
		return accessor.getAccessorNameInCollection(this);
	}

	private PersistenceManager persistenceManager() throws ClassNotFoundException
	{
		return this.owner.persistenceManager();
	}

	public final synchronized boolean removeElement(Object anObject)
		throws IllegalAccessException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   ClassNotFoundException,
			   InstantiationException,
			   SQLException
	{
	    //Removes anObject from the collection.
	    //Note that you MUST tell anObject to save itself for
	    //the change to be reflected in the database!

		boolean answer =  this.collection().removeElement(anObject);
		Method aSetter = anObject.getClass().getMethod(this.ownerAccessorNameFor(((PersistentObject)anObject)), new Class[] {Class.forName("com.ssi.persistence.model.PersistenceProxy")});
		aSetter.invoke(anObject, null);
		return answer;
	}

	public void save()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				InstantiationException,
				SQLException
	{
		Enumeration iterator = this.collection().elements();
		while (iterator.hasMoreElements())
		{
			ProxyInterface element = (ProxyInterface)iterator.nextElement();
			if (!element.isProxy())
			{
				((PersistentObjectInterface)iterator.nextElement()).save();
			}
		}
	}

	private void setCollection()
		throws InstantiationException,
			   ClassNotFoundException,
			   NoSuchMethodException,
			   IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		Vector myProxies = this.persistenceManager().proxiesForCollection(this);
		this.collection(myProxies);
	}

	public final int size()
		throws Exception
	{
		return this.collection().size();
	}
}