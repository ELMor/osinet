package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Vector;
import java.sql.SQLException;
//import com.ssi.persistence.ui.*;


public abstract class ReadPersistentObject extends PersistentObject
	implements	PersistentObjectInterface,
				ProxyInterface
{
/* Since the receiver is a read only object it always is persistent */

public void delete()
		throws ClassNotFoundException,
			   NoSuchMethodException,
			   SQLException
	{
	    //Does nothing since the receiver is read only we can not write nor delete it
	}

/**
 * This method was created in VisualAge.
 */
public boolean isPersistent()
{
	return true;
}

public void save()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				InstantiationException,
				SQLException
	{
	    	    //Does nothing since the receiver is read only we can not write nor delete it
	}
}