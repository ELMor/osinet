package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public class DBDoubleFieldType extends DBFieldType
{
	public String fieldClassName()
	{
		return "java.lang.Double";
	}
	
	public static boolean represents(Field aField)
	{
		try
		{
			return aField.getType() == Class.forName("java.lang.Double");
		}
		catch (ClassNotFoundException ex)
		{
			return false;
		}
	}

	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		aSetter.invoke(newInstance, new Double[] {aField.getDouble()});
	}
}