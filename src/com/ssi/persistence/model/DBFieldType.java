package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.beans.PropertyDescriptor;

public abstract class DBFieldType
{
	private Class[] fieldClassArray() throws ClassNotFoundException
	{
		return new Class[] { Class.forName(this.fieldClassName()) };
	}
	// Subclass Responsibility
	abstract public String fieldClassName();
	public Method getGetterMethodFrom(Class aClass, String methodName)
		throws	ClassNotFoundException,
				NoSuchMethodException
	{
		return aClass.getMethod(methodName, null);
	}
	public Method getSetterMethodFrom(Class aClass, String methodName)
		throws	ClassNotFoundException,
				NoSuchMethodException
	{
		return aClass.getMethod(methodName, this.fieldClassArray());
	}
	public Object getValueOf(Object anInstance, Method aGetter)
		throws IllegalAccessException,
			   InvocationTargetException
	{
		return aGetter.invoke(anInstance, null);
	}

	public String oidForQuery(String anOID)
		throws Exception
	{
		throw new Exception("Should not implement!");
	}

	/**
	 * This method was created in VisualAge.
	 * @return boolean
	 * @param aClass java.lang.Class
	 */
	public static boolean represents(Field aField)
	{
		return false;
	}

	public abstract void setValueTo(Object newInstance, Method aSetter, DBField afield, PersistenceAccessor anAccessor)
		throws	IllegalAccessException,
				InvocationTargetException,
				SQLException;
	/**
	 * This method was created in VisualAge.
	 * @return java.lang.String
	 */
	public String toString() {
		return getClass().getName();
	}
}