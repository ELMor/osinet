package com.ssi.persistence.model;

import java.util.*;
import java.lang.reflect.*;
import java.sql.*;

public class ProxyKiller
{
	private PersistenceManager persistenceManager;
	
	
	public ProxyKiller(PersistenceManager aManager)
	{
		this.persistenceManager = aManager;
	}
	
	
	public PersistenceManager persistenceManager()
	{
		return this.persistenceManager();
	}

	public Vector killProxiesInCollection(Vector aCollection)
		throws InstantiationException,
		   IllegalAccessException,
		   ClassNotFoundException,
		   InvocationTargetException,
		   NoSuchMethodException,
		   SQLException
	{
		//This method kills all the proxies in aCollection.
		//It assumes they are all from the same class of objects.
		//Answers a new Vector with all the real objects.

		Enumeration iterator = aCollection.elements();
		String oids = "";
		Class proxiesClass = null;
		while (iterator.hasMoreElements())
		{
			ProxyInterface nextElement = (ProxyInterface)iterator.nextElement();
			if (nextElement.isProxy())
			{
				proxiesClass = nextElement.classOfPrefix();
				if (oids.length() != 0)
				{
					oids = oids + ", ";
				}
				oids = oids + "'" + nextElement.oid() + "'";
			}
		}
		oids = "oid in (" + oids + ")";
		if(proxiesClass != null)
			{
				return this.persistenceManager().instancesOfWhere(proxiesClass, oids);
			}
			else
			{
				return new Vector();
			}
	}
}