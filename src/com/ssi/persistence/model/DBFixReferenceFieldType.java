package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;
//import com.ssi.persistence.ui.*;
import java.beans.PropertyDescriptor;

public class DBFixReferenceFieldType extends DBFieldType
{
	// Inst Vars
	DBFieldType type;
	PersistenceAccessor accessor;


	public DBFixReferenceFieldType(PersistenceAccessor anAccessor)
	{
		accessor = anAccessor;
	}

	public String fieldClassName()
	{
		return "com.ssi.persistence.model.ProxyInterface";
	}

	public Object getValueOf(Object anInstance, Method aGetter)
		throws IllegalAccessException,
			   InvocationTargetException
	{
	  try
	  {
		ProxyInterface reference = (ProxyInterface)aGetter.invoke(anInstance, null);
		if(reference == null)
		{
			return null;
		}
		else
		{
			return "'" + reference.oid() + "'";
		}
	  }
	  catch (Exception ex)
	  {
			//MessageViewer.show( ex.getMessage(), "DBForeignFieldType error!");
			return null;
	  }
	}

	public static boolean represents(Field aField)
	{
		try
		{
			return PersistentObject.isClassForeignPersistent(aField.getType());
		}
		catch (ClassNotFoundException ex)
		{
			return false;
		}
	}

	public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
		throws IllegalAccessException,
			   InvocationTargetException,
			   SQLException
	{
		String oid = aField.getString();
		if(oid != null)
		{
			ProxyInterface proxy = ((ProxyInterface)new PersistenceFixProxy(aField.getString(), accessor));
			aSetter.invoke(newInstance, new ProxyInterface[] {proxy});
		}
	}

	private DBFieldType type()
	{
		if(type == null)
		{
			type = new DBStringFieldType();
		}
		return type;
	}
}