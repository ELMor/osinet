package com.ssi.persistence.model;

import java.util.*;

/**
 * 
 */
 
public class AccessorCache {

	private static AccessorCache singleton;
	private Hashtable cache; 

	
	public static AccessorCache getInstance()
	{
		if(singleton == null)
		{
			singleton = new AccessorCache();
		}
		return singleton;
	}
	
	/**
	 * Se ha creado el siguiente constructor como private para evitar que se pueda
	 * instanciar esta clase y de esta forma lograr un objeto singleton.
	 * Debe usarse #getInstance() para conseguir la instancia de esta clase.
	 */
	private AccessorCache() {
		this.cache = new Hashtable();
	}

	private Hashtable cache()
	{
		return this.cache;
	}

	public PersistenceAccessor put(Object key, PersistenceAccessor value)    
	{
		return (PersistenceAccessor)this.cache().put(key, value);
	}

	public PersistenceAccessor get(Object key)
	{
		return (PersistenceAccessor)this.cache().get(key);
	}

}