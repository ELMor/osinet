package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;

public interface PersistentObjectInterface
{
	public abstract void delete()
		throws NoSuchMethodException,
			   ClassNotFoundException,
			   SQLException;

	public abstract boolean isPersistent();

	public abstract String oid();

	public abstract void oid(String anOID);

	public abstract String oidPrefix()throws ClassNotFoundException,
											 NoSuchMethodException;

	public void save()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException,
				InstantiationException,
				SQLException;

	public abstract String tableName()
		throws	IllegalAccessException,
				InvocationTargetException,
				ClassNotFoundException,
				NoSuchMethodException;
				
	public abstract PersistenceManager persistenceManager()
  		throws ClassNotFoundException;  
}