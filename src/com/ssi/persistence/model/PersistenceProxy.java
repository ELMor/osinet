package com.ssi.persistence.model;

import java.lang.reflect.*;
import java.sql.SQLException;

public class PersistenceProxy implements ProxyInterface
{
	//Inst Vars
	String oid;
	PersistenceAccessor persistenceAccessor;

	//Class Methods
	public PersistenceProxy(String anOID, PersistenceAccessor anAccessor)
	{
		oid = anOID;
		persistenceAccessor = anAccessor;
	}

	public Class classOfPrefix()
		throws ClassNotFoundException
	{
		return this.oidParser().classOfOID(this.oid());
	}

	public Class classRepresented()
		throws ClassNotFoundException
	{
		return Class.forName("com.ssi.persistence.model.ProxyInterface");
	}

	//Inst Methods
	public void fetchRealFor(PersistentObjectInterface anOwner,String aMessage)
		throws IllegalAccessException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   ClassNotFoundException,
			   InstantiationException,
			   SQLException,
			   Exception
	{
		 this.getMethodFor(anOwner, aMessage).invoke(anOwner, new ProxyInterface[] {((ProxyInterface)this.getRealObject())});
	}

	private Method getMethodFor(PersistentObjectInterface anOwner,String aMessage)
		throws NoSuchMethodException,
			   ClassNotFoundException
	{
		Class aClass = this.classRepresented();
		return anOwner.getClass().getMethod(aMessage, new Class[] {aClass});
	}

	private PersistentObjectInterface getRealObject()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException,
			   Exception
	{
		return this.persistenceAccessor().instanceOfOID(this.oid());
	}
	//
	// Testing
	//

	public boolean isProxy()
	{
		return true;
	}

	public String oid()
	{
		return oid;
	}

	public void oid(String anOID)
	{
		oid = anOID;
	}

	private OIDParser oidParser()
	{
		return this.persistenceAccessor().persistenceManager().oidParser();
	}

	private PersistenceAccessor persistenceAccessor()
	{
		return this.persistenceAccessor;
	}
}