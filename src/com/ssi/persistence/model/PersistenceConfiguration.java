package com.ssi.persistence.model;

public abstract class PersistenceConfiguration
{
	public abstract DBEngineType getDBEngineType();
	/*Abstract superclass of thepersistence configuration class
	needed to configure the persistence framework. Defi the
	database name to be used, the PersistenceFactory, OID format
	and sets the accessors to be used in the application to
	the persistence manager.*/

	abstract public String databaseName();

	abstract public PersistenceFactory persistenceFactory();

	abstract public String driverName();

	/**
	 * Returns the maximun number of connection to handle at the same time.
	 */
	abstract public int maxConnections();
	
	abstract public String getConnectionPassword();
	
	abstract public String getConnectionUser();	
	
	public int indexStartPosition()
	{
		return 0;
	}

	public int indexStopPosition()
	{
		return 6;
	}

	public int prefixStartPosition()
	{
		return 7;
	}

	public int prefixStopPosition()
	{
		return 9;
	}


	/**
	 * Configures the manager received as parameter. Set
	 * its accessors and connection pool.
	 */
	public void configurePersistenceManager(PersistenceManager aManager)
	{
		this.setConnectionPoolTo(aManager);
		this.setAccessorsTo(aManager);
	}

	public void setConnectionPoolTo(PersistenceManager aManager)
	{
		aManager.connectionPool(new ConnectionPool(this.connectionAttempts(), this.poolWaitTime(), aManager));

	}
	public void setAccessorsTo(PersistenceManager aManager)
	{
	    //Sets the accesso for the persistent object to be used
	    //by the application. Subclases must use super when
	    //redefining this method.
	    //Note that you could explicity specify a new cache for
	    //each accessor. See com.ssi.persistence.cache package.
	    
        aManager.addAccessor(new UserAccessor(aManager));
		//aManager.addAccessor(UserAccessor.getInstance(aManager));
	}


	/**
	 * Returns the number tries to make in order to connect to the DB.
	 */
	abstract public int connectionAttempts();


	/**
	 * Returns the number of milliseconds to hold
	 * a requester of a connection if no connections
	 * are available
	 */
	abstract public int poolWaitTime();

}