package com.ssi.persistence.cache;

import java.util.Hashtable;
import com.ssi.persistence.model.PersistentObjectInterface;

import com.ssi.util.*;public class PersistenceCache implements IPersistenceCache
{
	/* Caches persistence instanes. The keys are the OIDs and
	the values are the instances for those OIDs*/

	// Inst Vars
	Hashtable dictionary ;

	//Class methods
	public PersistenceCache()
	{
		dictionary = new Hashtable();
	}
	public Object at(Object aKey)
	{
		return this.dictionary().get(aKey);
	}
	public void atKeyPut(Object aKey, PersistentObjectInterface aValue)
	{
		if(aKey==null){
		 LogFile.log("PersistenceCache: recibiendo Object nulo");
		 return;
		}

		if(aValue==null){
		 LogFile.log("PersistenceCache: recibiendo PersistentObjectInterface nulo");
		 return;
		}
		
		this.dictionary().put(aKey, aValue);
	}
	// Instance Methods
	private Hashtable dictionary()
	{
		return dictionary;
	}
	public boolean includes(PersistentObjectInterface aValue)
	{
		return this.dictionary().contains(aValue);
	}
	public boolean includesKey(Object aKey)
	{
		return this.dictionary().containsKey(aKey);
	}
	public void remove(PersistentObjectInterface aValue)
	{
		this.dictionary().remove(aValue.oid());
	}
}