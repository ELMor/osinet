package com.ssi.persistence.cache;

import java.util.Hashtable;
import java.util.Vector;
import com.ssi.persistence.model.PersistentObjectInterface;

public class MRUCache implements IPersistenceCache
{
	/*Caches the most recently used objects up to the cache's
	maximun capacity. When the cache fills up the least used
	object is removed to make space for the newly cache objects*/

	// Inst Vars
	Vector mruVector;
	Hashtable dictionary ;
	int cacheLimit;         //Cache limit


	public MRUCache(int theCacheLimit)
	{
	    cacheLimit = theCacheLimit;
		dictionary = new Hashtable();
		mruVector = new Vector();
	}

	public Object at(Object aKey)
	{
		Object answer = this.dictionary().get(aKey);
		if (answer != null)
		{
		    this.moveFirst(answer);
		}
		return answer;
	}

	public void atKeyPut(Object aKey, PersistentObjectInterface aValue)
	{
	    if (this.isFull())
	    {
	        this.removeLast();
	    }
		this.addFirst(aKey, aValue);
	}

	private Hashtable dictionary()
	{
		return dictionary;
	}

	private Vector mruVector()
	{
		return mruVector;
	}

	private int cacheLimit()
	{
		return cacheLimit;
	}

	public boolean includes(PersistentObjectInterface aValue)
	{
		return this.dictionary().contains(aValue);
	}

	public boolean includesKey(Object aKey)
	{
		return this.dictionary().containsKey(aKey);
	}

	public void remove(PersistentObjectInterface aValue)
	{
	    this.mruVector().removeElement(aValue.oid());
		this.dictionary().remove(aValue.oid());
	}

	public void removeLast()
	{
	    Object oidToRemove = this.mruVector().lastElement();
	    this.mruVector().removeElement(oidToRemove);
	    this.dictionary().remove(oidToRemove);
	}

	public void addFirst(Object aKey, PersistentObjectInterface aValue)
	{
		this.mruVector().insertElementAt(aKey,0);
		this.dictionary().put(aKey, aValue);
	}

	public void moveFirst(Object aKey)
	{
		this.mruVector().removeElement(aKey);
		this.mruVector().insertElementAt(aKey,0);
	}

	public boolean isFull()
	{
		return this.mruVector().size() == this.cacheLimit();
	}
}