package com.ssi.persistence.cache;

import com.ssi.persistence.model.PersistentObjectInterface;

public interface IPersistenceCache
{
	/* Defines the cache interface for the caches
	to be used with the persistence framework.
	Objects are stored using a key/value pair.
	The keys are the OIDs and the values are
	the instances for those OIDs*/

	public abstract Object at(Object aKey);

	public abstract void atKeyPut(Object aKey, PersistentObjectInterface aValue);

	public abstract boolean includes(PersistentObjectInterface aValue);

	public abstract boolean includesKey(Object aKey);

	public abstract void remove(PersistentObjectInterface aValue);
}