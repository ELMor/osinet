package com.ssi.log.model;

public class NoLog extends ApplicationLog implements LogInterface
{
	public void clearLog()
	{
	}
	public void cr()
	{
	}
	public void crTab()
	{
	}
	public String loggedText()
	{
		return "No log enabled...";
	}
	public void logString(String aString)
	{
	}
	public void logStringCR(String aString)
	{
	}
	public void setLog(String aString)
	{
	}
	public void tab()
	{
	}
}