package com.ssi.log.model;

public interface LogInterface
{
	public void clearLog();
	public void cr();
	public void crTab();
	public String loggedText();
	public void logString(String aString);
	public void logStringCR(String aString);
	public void setLog(String aString);
	public void tab();
}