package com.ssi.bean;

import java.io.*;
import java.util.*;
import com.ssi.util.*;
import javax.servlet.*;
import com.ssi.util.LogFile;
import java.lang.reflect.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.ssi.http.*;
import com.ssi.model.salud.*;

public abstract class ServletSSI extends HttpServlet {
	public abstract ConeccionHTTP getConeccionHTTP(HttpServletRequest request, HttpServletResponse response)
			 throws Exception;
   /**
   * M�todo qu� obtiene el Bean al cu�l debe pedirle qu� resuelva los requerimientos qu�
   * han llegado del Navegador.
   */
   public abstract IView obtenerView(ConeccionHTTP unaConeccion);      

	/**
	* M�todo qu� obtiene el Bean qu� debe atender el requerimiento recibido como Par�metro y
	* delega su resoluci�n al mismo.
	*/
	public void procesarRequerimiento(ConeccionHTTP unaConeccion){
		LogFile.log("ServletSSI: procesarRequerimiento: Inicio.");
		IView oBean = null;
		if(unaConeccion.existsParamValue("Buscar")){
			LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
		}
		oBean = this.obtenerView(unaConeccion);
		oBean.resolverRequerimiento(unaConeccion, this);
		LogFile.log("ServletSSI: procesarRequerimiento: Fin.");
	} // END OF procesarRequerimiento
	
	public abstract void destroy();


	public void doPost(HttpServletRequest request, HttpServletResponse response){
		try
		{
			LogFile.log("ServletSSI: doPost: Inicio.");
			this.procesarRequerimiento(this.getConeccionHTTP(request, response));
		}
		catch (Exception ex){
			LogFile.log("ServletSSI: doPost: " + ex.getMessage());
		}
	} //fin metodo doPost()


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			LogFile.log("ServletSSI: doGet: Inicio.");
			this.procesarRequerimiento(this.getConeccionHTTP(request, response));
		}
		catch (Exception ex)
		{
			LogFile.log("ServletSSI: doGet: Se fue de pista!"+ ex.getMessage());
		}

	} //fin metodo doGet()


	public void mostrarJSP(ConeccionHTTP unaConeccion, String url){
		LogFile.log("ServletSSI: mostrarJSP: Inicio");
		ServletContext sc = getServletContext();
		RequestDispatcher rd = sc.getRequestDispatcher(url);
		LogFile.log("ServletSSI: mostrarJSP: url: "+url);
		try
		{
		   rd.forward(unaConeccion.getRequest(), unaConeccion.getResponse());
		}
		catch(ServletException se){
			LogFile.log("ServletSSI: mostrarJSP: se: "+se);
		}
		catch(IOException ioe){
			LogFile.log("ServletSSI: mostrarJSP: ioe: "+ioe);
		}
	} //END OF mostrarJSP
} //END OF class ServletSSI