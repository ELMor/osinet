package com.ssi.bean;

import com.ssi.http.*;

public abstract class View implements IView
{
	public abstract String URL();
	public DummyView getDummyView()
	{
		return new DummyView();
	}
	public abstract String obtenerURLDestino(ConeccionHTTP unaConeccion);
	
	public void resolverRequerimiento(ConeccionHTTP unaConeccion, ServletSSI myServlet)
	{
		String miURL = this.obtenerURLDestino(unaConeccion);
		myServlet.mostrarJSP(unaConeccion,miURL);
	}
}