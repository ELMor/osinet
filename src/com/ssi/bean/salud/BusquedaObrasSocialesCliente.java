package com.ssi.bean.salud;

import com.ssi.bean.View;
import java.util.Vector;
import com.ssi.persistence.model.*;
import com.ssi.util.LogFile;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.model.salud.SistemaNovahis;
/**
 * Insert the type's description here.
 * Creation date: (26/12/2000 03:15:09 p.m.)
 * @author: Administrator
 */
public class BusquedaObrasSocialesCliente extends View {
	String numeroPacienteReferenciado;

/**
 * Insert the method's description here.
 * Creation date: (26/12/2000 07:13:03 p.m.)
 * @return java.lang.String
 */
public java.lang.String getNumeroPacienteReferenciado() {
	return numeroPacienteReferenciado;
}
/**
 * obtenerURLDestino method comment.
 */
public String obtenerURLDestino(com.ssi.http.ConeccionHTTP unaConeccion) {
	return this.URL();
}
/**
 * Insert the method's description here.
 * Creation date: (26/12/2000 07:13:03 p.m.)
 * @param newNumeroPacienteReferenciado java.lang.String
 */
public void setNumeroPacienteReferenciado(java.lang.String newNumeroPacienteReferenciado) {
	numeroPacienteReferenciado = newNumeroPacienteReferenciado;
}
/**
 * URL method comment.
 */
public String URL() {
	return "/jsp/ObrasSociales.jsp";
}

 

public JDBCAnswerResultSet ejecutarConsulta () {
	try {
			PersistenceManager manager = SistemaNovahis.getInstance().getPersistenceManager();
			return (JDBCAnswerResultSet)manager.executeQuery(getQueryString());
	}
	catch(Exception e) {
		LogFile.log(e.toString());
		return null;
	}
}public String getQueryString() {
	return "SELECT garantes.nombre_garante, " +
		"cliente_pagadores.clipag_numpoliza, " +
		"co_planes.plan_desc, " +
		"co_cli_planes.num_afil, " + 
		"co_cli_planes.factivo_desd, " +
		"co_cli_planes.factivo_hast " +
	"FROM cliente_pagadores, " +
		"garantes, " +
		"pagadores, " +
		"co_cli_planes, " +
		"co_planes " +
	"WHERE garantes.codigo_garante_pk = pagadores.codigo_garante_pk " +
		"and pagadores.cod_pagador_pk = cliente_pagadores.cod_pagador_pk " +
		"and cliente_pagadores.clipag_pk = co_cli_planes.clipag_pk " +
		"and co_planes.plan_pk = co_cli_planes.plan_pk " +
		"and (cliente_pagadores.codigo_cliente = " + getNumeroPacienteReferenciado() + " )";
}}