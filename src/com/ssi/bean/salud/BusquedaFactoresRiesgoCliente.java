package com.ssi.bean.salud;

import com.ssi.bean.View;
import java.util.Vector;
import com.ssi.persistence.model.*;
import com.ssi.util.LogFile;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.model.salud.SistemaSiapwin;
/**
 * Insert the type's description here.
 * Creation date: (26/12/2000 03:15:09 p.m.)
 * @author: Administrator
 */
public class BusquedaFactoresRiesgoCliente extends View {
	String numeroPacienteReferenciado;
public JDBCAnswerResultSet ejecutarConsulta () {
	try {
			PersistenceManager manager = SistemaSiapwin.getInstance().getPersistenceManager();
			return (JDBCAnswerResultSet)manager.executeQuery(getQueryString());
	}
	catch(Exception e) {
		LogFile.log(e.toString());
		return null;
	}
}
/**
 * Insert the method's description here.
 * Creation date: (28/12/2000 07:38:38 p.m.)
 * @return java.lang.String
 */
public java.lang.String getNumeroPacienteReferenciado() {
	return numeroPacienteReferenciado;
}
public String getQueryString() {
    String result = "select tfr.nom_fr, hfr.fecha " +
	"from tfac_rie tfr, hfac_rie hfr " +
	"where hfr.activo = 1 " +
		"and tfr.cod_fr = hfr.cod_fr " +
		"and hfr.codigo_cliente =" + getNumeroPacienteReferenciado() ;
		//"and hfr.codigo_cliente =" + getNumeroPacienteReferenciado() + " )";
	LogFile.log("getQueryString(): " + result);
	return result;	
}
/**
 * obtenerURLDestino method comment.
 */
public String obtenerURLDestino(com.ssi.http.ConeccionHTTP unaConeccion) {
	return this.URL();
}
/**
 * Insert the method's description here.
 * Creation date: (28/12/2000 07:38:38 p.m.)
 * @param newNumeroPacienteReferenciado java.lang.String
 */
public void setNumeroPacienteReferenciado(java.lang.String newNumeroPacienteReferenciado) {
	numeroPacienteReferenciado = newNumeroPacienteReferenciado;
}
/**
 * URL method comment.
 */
public String URL() {
	return "/jsp/FactoresRiesgoCliente.jsp";
}
}