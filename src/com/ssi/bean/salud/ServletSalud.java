package com.ssi.bean.salud;

import com.ssi.util.*;
import com.ssi.bean.*;
import com.ssi.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.reflect.*;
import com.ssi.http.salud.*;
import com.ssi.util.LogFile;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;
import com.ssi.model.salud.*;


public class ServletSalud extends ServletSSI {
	
	private PersistenceManager persistenceManager;
	private SistemaSalud sistemaNovahis;
	private SistemaSalud sistemaSiapwin;


	public IView getDummyView()
	{
		return new DummyView();
	}    
	
	public IView getLoginView()
	{
		return new Login();
	}
   
   
   /**
   * M�todo qu� obtiene el Bean al cu�l debe pedirle qu� resuelva los requerimientos qu�
   * han llegado del Navegador.
   */
   public IView obtenerView(ConeccionHTTP unaConeccion){
		if(unaConeccion.existsParamValue("view")) {
			try{
				String valueView = unaConeccion.getSingleParamValue("view");
				LogFile.log("ServletSalud: obtenerView: valueView: "+valueView);
				return (IView)unaConeccion.getFromSession(valueView);
			}
			catch (Exception ex) {
				LogFile.log("ServletSalud: obtenerView: Excepcion: "+ex);
				LogFile.log("ServletSalud: obtenerView: llamo a DummyView...");
				return this.getDummyView();
			}
		}
		else {
			return this.getLoginView();
		}
	} // END OF obtenerView()


   /**
   * M�todo qu� devuelve ConeccionHTTP
   * 
   */
	 public ConeccionHTTP getConeccionHTTP(HttpServletRequest request, HttpServletResponse response)
		throws Exception
	{
		return new ConeccionHTTPSalud(request, response);
	}


	/**
	* M�todo qu� conecta el Servlet a la Base de Datos Access.
	*/
	public void connect() {
		LogFile.log("ServletSalud: connect: Inicio.");
		PersistenceManager persistenceManager = new PersistenceManager(new BrokerSaludConfiguration());
		try{
			persistenceManager.connect();

		} catch(SQLException sqle){
			LogFile.log("ServletSalud: connect: Se produjo una SQLExcepcion.");
		} catch(Exception e){
			LogFile.log("ServletSalud: connect: Se produjo una Excepcion.");
		}
		LogFile.log("ServletSalud: connect: Fin.");
	} // END OF connect


	/**
	* M�todo qu� conecta el Servlet a la Base de Datos Novahis.
	*/
	public void connectNovahis() {
		LogFile.log("ServletSalud: connectNovahis: Inicio.");
			 SistemaSalud pmNovahis = SistemaNovahis.getInstance();
		try{
			pmNovahis.connect();

		} catch(SQLException sqle){
			LogFile.log("ServletSalud: connectNovahis: Se produjo una SQLExcepcion.");
		} catch(Exception e){
			LogFile.log("ServletSalud: connectNovahis: Se produjo una Excepcion.");
		}
		LogFile.log("ServletSalud: connectNovahis: Fin.");
	} // END OF connect


	/**
	* M�todo qu� conecta el Servlet a la Base de Datos Siapwin.
	*/
	public void connectSiapwin() {
		LogFile.log("ServletSalud: connectSiapwin: Inicio.");
			SistemaSalud pmSiapwin = SistemaSiapwin.getInstance();
		try{
			pmSiapwin.connect();

		} catch(SQLException sqle){
			LogFile.log("ServletSalud: connectSiapwin: Se produjo una SQLExcepcion.");
		} catch(Exception e){
			LogFile.log("ServletSalud: connectSiapwin: Se produjo una Excepcion.");
		}
		LogFile.log("ServletSalud: connectSiapwin: Fin.");
	} // END OF connect



	/**
	* M�todo qu� abre el archivo de logueo.
	*/
	public void openLog()
	{
		Parametros.inicializarVariablesStaticas();
		// verificar que se carg� bien el archivo .ini
		// usar Parametros.LOG_FILENAME.length() como la prueba
		// si el archivo no exist�a, dar� un NullPointerException
		try {
			Parametros.LOG_FILENAME.length() ;
		} catch (Exception e) {
		}

		LogFile.openLogFile(Parametros.LOG_FILENAME);
		LogFile.log("ServletSalud: openLog: Reinici� el servlet");
		//esto es por caso que en el codigo, Parametros.LOG_TO_FILE = true
		// pero en el archivo plano, Parametros.LOG_TO_FILE = false.  Entonces
		// necesitamos cerrar el archivo.
		if (!Parametros.LOG_TO_FILE){
			LogFile.log("ServletSalud: openLog: Se intenta cerrar el archivo log");
			Parametros.LOG_TO_FILE=true;
			LogFile.close();
		}
	} //END OF openLog


/**
* M�todo qu� inicializa el Servlet; abre el Log y conecta el Servlet con la Base de Datos.
*/
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.openLog();
		//this.connect();
		this.connectNovahis();
		this.connectSiapwin();
	}//END OF init()
	
	
	public void setPersistenceManager(PersistenceManager unManager) {
		persistenceManager = unManager;
	}

	public PersistenceManager getPersistenceManager(){
		return persistenceManager;
	}

   
	/**
	* Destruye el Servlet. Cierra las conecciones con la Base de Datos.
	*/
	public void destroy() {
		try {
			LogFile.log("=========================================================");
			LogFile.log("*************** FINALIZANDO SERVLET SALUD ***************");
			LogFile.log("=========================================================");
			this.getPersistenceManager().closeConnection();
		}
		catch (Exception e) {
		}
	}
}// END OF class ServletSalud