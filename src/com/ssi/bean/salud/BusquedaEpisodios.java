package com.ssi.bean.salud;

/**
* Clase qu� se encarga de resolver las necesidades de la Pantalla de Episodios (JSP).
*
* @see com.ssi.bean.#View
*/
import java.io.*;
import java.sql.*;
import java.util.*;
import com.ssi.bean.*;
import com.ssi.http.*; 
import com.ssi.util.*;
import com.ssi.http.*;
import javax.servlet.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import com.ssi.http.salud.*;
import com.ssi.model.salud.*;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

public class BusquedaEpisodios extends View {
	
	private Vector listaEpisodios;
	private Vector pacientes;

	public void setPacientes (Vector unVectorPacientes){
		this.pacientes = unVectorPacientes;
	} //END OF setPacientes

	public Vector getPacientes (){
		 return this.pacientes;
	}

	public Vector getEpisodios(){
		return listaEpisodios;
	}

	public void setEpisodios(Vector unaLista){
		listaEpisodios = unaLista;
	}
	
	public static View getView(ConeccionHTTP unaConeccion){
		View newView = null;
		try {
			if (unaConeccion.existsObjectInSession("BusquedaEpisodios")){
				newView = (View)unaConeccion.getObjectFromSession("BusquedaEpisodios");
			}
			else {
				newView = new BusquedaEpisodios();
				unaConeccion.putObjectInSession("BusquedaEpisodios", newView);
			}
		}catch (Exception exc) {
			LogFile.log("BusquedaEpisodios: getView: exc: "+exc);
		}
		return newView;
	} // END OF getView

	/**
	* M�todo qu� devuelve la URL del JSP qu� se debe mostrar seg�n lo qu� resolvi� el Bean
	* par aun requerimiento dado.
	*
	* @param unaConeccion  Instancia de la Clase ConeccionHTTP.
	*
	* @return answer       Cadena de Caracteres que simboliza la URL del JSP que se debe mostrar.
	*/
	public String obtenerURLDestino(ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEpisodio: obtenerURLDestino: Inicio ");
		try {
			if(unaConeccion.existsParamValue("episodioNumero") &&
				 !(unaConeccion.getSingleParamValue("episodioNumero").equals("inicial"))){
			   LogFile.log("BusquedaEpisodio: obtenerURLDestino: se clicke� un link...");
			   try{
					View busquedaEncuentros = BusquedaEncuentros.getView(unaConeccion);
					this.setearEpisodios((BusquedaEncuentros) busquedaEncuentros, unaConeccion);
					this.setearEncuentros((BusquedaEncuentros) busquedaEncuentros, unaConeccion);
					unaConeccion.putInSession("busquedaEncuentros", busquedaEncuentros);
					return BusquedaEncuentros.getURL();
				} catch (Exception ex){
					LogFile.log("BusquedaEpisodio: obtenerURLDestino: Me pinche!"+ex);
				}
			} // ENDif
		} catch(Exception exc){
			LogFile.log("BusquedaEpisodio: obtenerURLDestino: Excepcion = "+exc);
		}
		LogFile.log("BusquedaEpisodio: obtenerURLDestino: fin & url "+this.getURL());
		LogFile.log("BusquedaEpisodio: obtenerURLDestino: Fin ");
		return this.getURL();
	} // END OF obtenerURLDestino

	/**
	* M�todo Est�tico qu� devuelve la URL del Bean.
	*/
	public static String getURL(){
		return "/jsp/episodios.jsp";
	} // END OF getURL

	/**
	* M�todo qu� devuelve la URL del Bean.
	*/
	public String URL(){
		return "/jsp/episodios.jsp";
	} // END OF getURL

/**
* M�todo que construye la Restricci�n a la Base de Datos
*/
	public String construirRestriccion(){
		LogFile.log("BusquedaEpisodio construirRestriccion Inicio...");
		
		return "";
	}
	/**
	* M�todo que devuelve un Vector que contiene los Episodios para un Paciente determinado
	* de la Base de Datos del Sistema Siapwin y Novahis.
	*
	* @return Vector   Listado de los Episodios de un Paciente.
	*/
	public void obtenerEpisodios(ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEpisodio: obtenerEpisodios: Inicio.");
		Vector vEpisodios = new Vector();
		try {
			vEpisodios = Episodio.getEpisodios(this.getPacientes());
			this.setEpisodios(vEpisodios);
		}catch (Exception e){
			LogFile.log("BusquedaEpisodio: obtenerEpisodios: Exception: "+e);
		}
		LogFile.log("BusquedaEpisodio: obtenerEpisodios: Fin.");
	} // END OF obtenerEpisodios()

	public void obtenerTodosLosEpisodios(ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaEpisodio: obtenerTodosLosEpisodios: Inicio.");
	  this.obtenerEpisodios(unaConeccion);
	} // END OF obtenerEpisodios()

/**
* M�todo que 
*/
	private void setearEpisodios(BusquedaEncuentros busquedaEncuentros, ConeccionHTTP unaConeccion){
		LogFile.log("BusquedaEpisodios: setearEpisodios: Inicio");
		Vector vEpisodios = new Vector();
		try {
			vEpisodios.addElement((String) unaConeccion.getSingleParamValue("episodioNumero"));
		}catch (Exception exc){
			LogFile.log("BusquedaEpisodios: setearEpisodios: Excepcion al buscar el episodio.");
		}
		busquedaEncuentros.setearEpisodios(vEpisodios);
	} //END OF setearEpisodios

/**
*
*/
	private void setearEncuentros(BusquedaEncuentros busquedaEncuentros, ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaEpisodios: setearEncuentros: Inicio");
		busquedaEncuentros.obtenerEncuentros(unaConeccion);
	} // END OF setearEncuentros


/**
 * Insert the method's description here.
 * Creation date: (16/02/01 12:44:58)
 */

 public EpisodioSalud buscarEpisodio(String idEpisodio,String idSistema) {
	
	 Enumeration e = listaEpisodios.elements();
	 EpisodioSalud episodio = null;
	
	 while(e.hasMoreElements()){
	  episodio= (EpisodioSalud)e.nextElement();
	  if(episodio.ve_ep_ref().equals(idEpisodio) && episodio.sistema().nombreSistema().equals(idSistema))
	   return episodio;
	 }
	
	 return episodio; 
  	  
	}} // END OF Class BusquedaEpisodios