package com.ssi.bean.salud;

import com.ssi.bean.*;
import com.ssi.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import java.lang.reflect.InvocationTargetException;
import com.ssi.http.*;
import com.ssi.http.salud.*;

/**
* Clase de tipo Bean qu� se utiliza para el manejo de la pantalla de Login.
*/
public class Login extends View {
	private String nombreUsuario;
	private String contrasenia;
	private String mensaje = "Bienvenido...";


/**
* M�todo qu� devuelve la URL del Bean.
*/
	public String URL(){
		return Login.getURL();
	} // END OF getURL

 /**
 * M�todo Est�tico qu� devuelve la URL del Bean.
 */
	public static String getURL(){
		return "/jsp/login.jsp";
	} // END OF getURL

 /**
 * M�todo Est�tico qu� devuelve la URL del Bean.
 */
	public void obtenerInformacionUsuario(ConeccionHTTP unaConeccion){
		LogFile.log("Login: obtenerInformacionUsuario: Inicio");
		try {
				this.setNombreUsuario(unaConeccion.getSingleParamValue("tfUsuario"));
				this.setContrasenia(unaConeccion.getSingleParamValue("tfContrasenia"));
				LogFile.log("Login: obtenerInformacionUsuario: ya seteo las var");
		}
		catch (Exception ex){
			this.setNombreUsuario(null);
			this.setContrasenia(null);
			LogFile.log("Login : obtenerInformacionUsuario : " + ex);
		}
	} // END OF obtenerInformacionUsuario()

	/**
	* M�todo qu� devuelve la URL del Bean.
	*/
	public void reIniciarMensajes(){
		this.setMensaje("Bienvenido...");
	} // END OF reIniciarMensajes

	public void setMensaje(String unMensaje){
		this.mensaje = unMensaje;
	}

	public String getMensaje(){
		if (this.mensaje==null){
			this.mensaje = new String();
		}
		return this.mensaje;
	}

	public String obtenerURLDestino(ConeccionHTTP unaConeccion){
		String answer = Login.getURL();
		this.obtenerInformacionUsuario(unaConeccion);
		if (this.puedeIntentarLogin()){
			answer = this.efectuarLogin(unaConeccion);
		}
		LogFile.log("Login: obtenerURLDestino: Mensaje: "+this.getMensaje());
		return answer;
	}

/**
 * Metodo que efectua el login de un usuario al sistema. En caso de
 * no poder validar al usuario se avisa con un mensaje y se da la
 * posibilidad de un nuevo intento.
 */

	public String efectuarLogin(ConeccionHTTP unaConeccion)
	{
    System.out.println("hola login");
    LogFile.log("Login.efectuarLogin. inicio");
		String answer = Login.getURL();
		User elUsuario = null;
		try {
		    String aUserName = this.getNombreUsuario();
			elUsuario = User.userNamed(this.getNombreUsuario());

		}catch (SQLException sqle){
			LogFile.log("efectuarLogin : Error de Base de Datos: "+sqle);
			LogFile.log(sqle);
			this.setMensaje("Error de Base de Datos");
		}catch (Exception e){
			LogFile.log("efectuarLogin : Exception: "+e);
			LogFile.log(e);
			this.setMensaje("Exception");
		}
		if (elUsuario != null){
			if(elUsuario.password().equals(this.getContrasenia()))
			{
				((ConeccionHTTPSalud)unaConeccion).setLoggedUser(elUsuario);
				answer = BusquedaPaciente.getURL(); //"/jsp/BuscarPacientes.jsp"
				this.reIniciarMensajes();
				this.setNombreUsuario(new String());
				this.setContrasenia(new String());
			}
			else
			{
				this.setMensaje("Contrase�a no v�lida.");
				this.setContrasenia(new String());
			}
		}
		else
		{
			this.setMensaje("Usuario no encontrado. Corrija el nombre.");
			this.setNombreUsuario(new String());
			this.setContrasenia(new String());
		}
		return answer;

	}

	public boolean puedeIntentarLogin()
	{
		return !((this.getNombreUsuario()==null) || (this.getContrasenia()==null));
	}

	public void setContrasenia(String unaContrasenia)
	{
		this.contrasenia = unaContrasenia;
	}

	public String getContrasenia()
	{
		if (this.contrasenia==null){
			this.contrasenia = new String();
		}
		return contrasenia;
	}
	public void setNombreUsuario(String unNombre)
	{
		this.nombreUsuario = unNombre;
	}
	public String getNombreUsuario()
	{
		if (this.nombreUsuario==null){
			this.nombreUsuario = new String();
		}
		return nombreUsuario;
	}

} // END OF CLASS Login