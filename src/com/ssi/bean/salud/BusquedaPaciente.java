package com.ssi.bean.salud;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.ssi.bean.*;
import com.ssi.http.*; 
import com.ssi.util.*;
import com.ssi.http.*;
import javax.servlet.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import com.ssi.http.salud.*;
import com.ssi.model.salud.*;
import java.sql.SQLException;
import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

/**
* 
*/
public class BusquedaPaciente extends View {
	private String rfc;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String fechaNacimiento;
	private Vector listaPacientes;
	private String mensajeExcesoPacientes;
	private static int limitePacientes = 5;
	/**
	* Constructor
	*/
	public BusquedaPaciente() {
		rfc              = "";
		nombre           = "";
		apellido1        = "";
		apellido2        = "";
		fechaNacimiento  = "";
		mensajeExcesoPacientes = "Se han encontrado demasiados resultados, si el paciente no est&aacute; en este listado, busque nuevamente completando m&aacute;s datos.";
		
		//Esta variable sirve para limitar la busqueda.
		limitePacientes  = 1000; 
	
	
	
	} // END OF BusquedaPaciente()
/**
*
*/
public String getApellido1(){
	return this.apellido1;
} // END OF getApellido1()
/**
*
*/
public String getApellido2(){
	return this.apellido2;
} // END OF getApellido2()
/**
*
*/
public String getFechaNac(){
	return this.fechaNacimiento;
} // END OF getFechaNac()
	/**
	* M�todo que devuelve el limite permitido de Pacientes qu� se van a mostrar en Pantalla.
	*
	* @return int  Valor que representa el limite de los Pacientes qu� se puede mostrar en pantalla.
	*/
	public int getLimitePacientes(){
		return limitePacientes;
	}
/**
*
*/
	public Vector getListaPacientes(){
		return listaPacientes;
	}
	/**
	*
	*/
	public String getMensajeExcesoPacientes(){
		return mensajeExcesoPacientes;
	}
/**
*
*/
public String getNombrePaciente(){
	return this.nombre;
} // END OF getNombrePaciente()
/**
*
*/
public String getRFC(){
	return this.rfc;
} // END OF getRFC()
	/**
	* M�todo Est�tico qu� devuelve la URL del Bean.
	*/
	public static String getURL(){
		return "/jsp/buscarPacientes.jsp";
	} // END OF getURL
	public static String getURLResultados(){
		return "/jsp/resultadosPacientes.jsp";
	} // END OF getURLResultados()
 /**
 * M�todo Est�tico qu� devuelve la URL del Bean.
 */

	public void obtenerInformacionPantalla(ConeccionHTTP unaConeccion){
		LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: Inicio");
		try {
			this.setNombrePaciente(unaConeccion.getSingleParamValue("tfNombre").toUpperCase().trim());
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfNombre: "+getNombrePaciente());

			this.setApellido1(unaConeccion.getSingleParamValue("tfApellido1").toUpperCase().trim());
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfApellido1: "+getApellido1().trim());

			this.setApellido2(unaConeccion.getSingleParamValue("tfApellido2").toUpperCase().trim());
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfApellido2: "+getApellido2());

			this.setRFC(unaConeccion.getSingleParamValue("tfRFC").trim());
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfRFC: "+getRFC());

			this.setFechaNac(unaConeccion.getSingleParamValue("tfFechaNac").trim());
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: tfFechaNac: "+getFechaNac());
		}
		catch (Exception ex) {
			this.setNombrePaciente(null);
			this.setApellido1(null);
			this.setApellido2(null);
			this.setRFC(null);
			this.setFechaNac(null);
			LogFile.log("BusquedaPaciente: obtenerInformacionPantalla: " + ex);
		}
	} // END OF obtenerInformacionUsuario()
	/**
	* obtengo vector de pacientes
	*/
	public void obtenerPacientes(ConeccionHTTP unaConeccion) {
	  LogFile.log("BusquedaPaciente: obtenerPacientes: Inicio.");
	  Vector vPacientes = new Vector();
	  try {
		this.obtenerInformacionPantalla(unaConeccion);
		vPacientes = Paciente.getPacientes(this.getRFC(),this.getNombrePaciente(), this.getApellido1(), this.getApellido2(), this.getFechaNac());
		this.setListaPacientes(vPacientes);
	  }catch (SQLException sqle) {
		  LogFile.log("BusquedaPaciente: obtenerPacientes: SqlException "+sqle);
	  } catch (Exception e) {
		LogFile.log("BusquedaPaciente: obtenerPacientes: Exception: "+e);
	  }
	} // END OF obtenerPacientes()
	/**
	* M�todo Est�tico qu� devuelve la URL del Bean.
	*/
	public String obtenerURLDestino(ConeccionHTTP unaConeccion) {
		// Antes obten�a a DummyView y le ped�a el URL
		try {
		if(unaConeccion.existsParamValue("btbuscarlupa")) {
			if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("inicial")){
				return Login.getURL();
			}
			if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("verEpisodios")){
				View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
				this.setearPacientes((BusquedaEpisodios) busquedaEpisodios, unaConeccion);
				this.setearEpisodios((BusquedaEpisodios) busquedaEpisodios, unaConeccion);
				unaConeccion.putInSession("busquedaEpisodios", busquedaEpisodios);
				return busquedaEpisodios.URL();
			}
			//ALberto modif. 21/12/00
		    if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("verMenu")){
			  View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
			  this.setearPacientes((BusquedaEpisodios) busquedaEpisodios, unaConeccion);
			  unaConeccion.putInSession("busquedaEpisodios", busquedaEpisodios);
			  LogFile.log("BusquedaMenu: obtenerURLDestino: Inicio ");
			  return "/jsp/menu.jsp";
		    }

  	
  		    //LS modif. 5/11/01
			if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("BuscarRapida")){
				LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
				try{
					LogFile.log("BusquedaPaciente: obtenerURLDestino: llama a obtenerPacientes.");
					this.obtenerPacientes(unaConeccion);
					return this.getURL();
				} catch (Exception ex){
					LogFile.log("BusquedaPaciente: obtenerURLDestino: Me pinche!"+ex);
				}
        } // ENDif
  	
  	
			if(unaConeccion.getSingleParamValue("btbuscarlupa").equals("Buscar")){
				LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
				try{
					LogFile.log("BusquedaPaciente: obtenerURLDestino: llama a obtenerPacientes.");
					this.obtenerPacientes(unaConeccion);
					return this.getURLResultados();
				} catch (Exception ex){
					LogFile.log("BusquedaPaciente: obtenerURLDestino: Me pinche!"+ex);
				}
			} // ENDif
		} // ENDif
		}
		catch (Exception ex){
			LogFile.log("BusquedaPaciente: obtenerURLDestino: Cojonudo pinchazo!"+ex);
		}
		LogFile.log("BusquedaPaciente: obtenerURLDestino: fin & url "+this.getURL());
		return this.getURL();
	} // END OF obtenerURLDestino
/**
*
*/
public void setApellido1(String _apellido1){
	this.apellido1 = _apellido1;
}
/**
*
*/
public void setApellido2(String _apellido2){
	this.apellido2 = _apellido2;
}
	/**
	* M�todo qu� setea los Episodios para los Pacientes seleccionados de la Lista; que luego
	* ser�n mostrados en la Pantalla de Episodios.
	*
	*/
	private void setearEpisodios(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion) {
		LogFile.log("BusquedaPaciente: setearEpisodios: Inicio");
		busquedaEpisodios.obtenerEpisodios(unaConeccion);
	} // END OF setearEpisodios
/**
* M�todo que obtiene el valor del identificador �nico del Paciente qu� ha sido seleccionado
* de la lista de Pacientes del Sistema Novahis.
* 
*/
	public String setearPacienteNovahis(ConeccionHTTP unaConeccion) {
		String idNovahis = "";
		try {
			if (!(unaConeccion.getSingleParamValue("checkNovahis").equals("cero"))) {
				LogFile.log("BusquedaPaciente: setearPacientes: busco por Novahis.");
				idNovahis = unaConeccion.getSingleParamValue("checkNovahis");
			}
		}catch (Exception exc) {
			LogFile.log("BusquedaPaciente: setearPacientes: sale en Novahis " + exc);
		}
		return idNovahis;
	} // END OF setearPacienteNovahis
/**
* M�todo que setea los valores de los identificadores �nicos de los Paciente qu� han sido
* seleccionado de la lista de Pacientes de su respectivo Sistema.
* 
*/
  private void setearPacientes(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion) {
	 
		LogFile.log("BusquedaPaciente: setearPacientes: Inicio");
		Vector vectorPacientes = new Vector();
		String idNovahis = setearPacienteNovahis(unaConeccion);
		String idSiapwin = setearPacienteSiapwin(unaConeccion);
		LogFile.log("idNovahis: " + idNovahis + " idSiapwin: " + idSiapwin);
		if(!idNovahis.trim().equals(""))
		{
			PacienteNovahis pNovahis = new PacienteNovahis();
			pNovahis.codigoCliente(idNovahis);
			vectorPacientes.addElement(pNovahis);
		}
		if(!idSiapwin.trim().equals(""))
		{
			PacienteSiapwin pSiapwin = new PacienteSiapwin();
			pSiapwin.codigoCliente(idSiapwin);
			vectorPacientes.addElement(pSiapwin);
		}
		busquedaEpisodios.setPacientes(vectorPacientes);
		LogFile.log("BusquedaPaciente: setearPacientes: Fin.");
		
	} // END OF setearPacientes
	/**
	* M�todo que obtiene el valor del identificador �nico del Paciente qu� ha sido seleccionado
	* de la lista de Pacientes del Sistema Novahis.
	*/
	public String setearPacienteSiapwin(ConeccionHTTP unaConeccion) {
		String idSiapwin = "";
		try {
			if (!(unaConeccion.getSingleParamValue("checkSiapwin").equals("cero"))) {
				LogFile.log("BusquedaPaciente: setearPacientes: busco por Siapwin.");
				idSiapwin = unaConeccion.getSingleParamValue("checkSiapwin");
			}
		}catch (Exception exc) {
			LogFile.log("BusquedaPaciente: setearPacientes: sale en Siapwin " + exc);
		}
		return idSiapwin;
	} // END OF setearPacienteNovahis
/**
*
*/
public void setFechaNac(String _fechaNac){
	this.fechaNacimiento = _fechaNac;
} // END OF setFechaNac()
/**
*
*/
	public void setListaPacientes(Vector unaLista){
		listaPacientes = unaLista;
	}
/**
* M�todo qu� devuelve el Nombre del Paciente qu� se quiere buscar y qu� fu� ingresado
* por el usuario en la pantalla de B�squeda de Pacientes.
*/
public void setNombrePaciente(String _nombre){
	this.nombre = _nombre;
} // END OF setNombrePaciente(String _nombre)
/**
*
*/
public void setRFC(String _rfc){
	this.rfc = _rfc;
}
	/**
	* M�todo qu� devuelve la URL del Bean.
	*/
	public String URL(){
		return BusquedaPaciente.getURL();
	} // END OF getURL
/**
 * Insert the method's description here.
 * Creation date: (16/02/01 12:14:50)
 */

 public PacienteSalud buscarPaciente(String idPaciente) {
	
	Enumeration e = this.listaPacientes.elements();
	PacienteSalud unPaciente=null;
	
	while(e.hasMoreElements()) {
	    
	   unPaciente = (PacienteSalud)e.nextElement();
	   if(unPaciente.codigoCliente().equals(idPaciente))
	    return unPaciente;
	}      
	   LogFile.log("BusquedaPaciente:No se encontr� paciente con id ="+idPaciente);

	  return unPaciente; 
	
	}} // END OF Class BusquedaPaciente