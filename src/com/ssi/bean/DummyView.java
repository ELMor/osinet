package com.ssi.bean;

import com.ssi.http.*;
import com.ssi.bean.*;

public class DummyView extends View
{

/**
* M�todo qu� devuelve la URL del Bean.
*/
	public String URL(){
		return DummyView.getURL();
	} // END OF getURL


	public static String getURL()
	{
		return "/jsp/dummyView.jsp";
	}
	
	public String obtenerURLDestino(ConeccionHTTP unaConeccion)
	{
		return this.getURL();
	}
}