package com.ssi.bean;

import com.ssi.http.*;
import com.ssi.bean.*;

public class MessageView extends View
{
	private String message;
	private String nextURL;

/**
* M�todo qu� devuelve la URL del Bean.
*/
	public String URL(){
		return MessageView.getURL();
	} // END OF getURL

	public static String getURL()
	{
		return "/jsp/messageView.jsp";
	}

	public String obtenerURLDestino(ConeccionHTTP unaConeccion)
	{
		String answer = this.nextURL();
		if (answer == null)
		{
			this.message("No se defini� una p�gina de continuaci�n!");
			answer = this.getURL();
		}
		return answer;
	}

	public String message()
	{
		return this.message;
	}

	public void message(String aMessage)
	{
		this.message = aMessage;
	}

	public String nextURL()
	{
		return this.nextURL;
	}

	public void nextURL(String anURL)
	{
		this.nextURL = anURL;
	}

}