package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public class EpisodioSiapwin extends EpisodioSalud {

			private String ve_ep_fecha      ;// 10 YMD
			private String ve_ep_ref        ;// 15 L
			private String ve_ep_hora       ;// 12 L
			private String ve_ep_servicio   ;//40 L
			private String ve_ep_cond       ;// 120 L
			private String ve_ep_estado     ;// 6 N
			private String ve_ep_idclient   ;// 15 L
			private String ve_ep_resolucion ;// 120 L






/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Episodio
* por defecto.
*/
	public EpisodioSiapwin(){
		super();
	} // END OF Constructor Episodio



	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return EpisodioSiapwin.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaSiapwin.getInstance();
	}

	public String centroDescripcion(){
		return "s/d.";
	}

	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
			LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre = '"+nombre+ "'";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 2.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre = '"+nombre+"'";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 2.2: "+ unaRestriccion);
			}
		}
		if (apellido1 != null && apellido1.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido1 = '"+apellido1+ "' ";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 3.1: "+ unaRestriccion);
			}
			else{
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido1 = '"+apellido1+ "'";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 3.2: "+ unaRestriccion);
			}
		}
		if (apellido2 != null && apellido2.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido2 = '"+apellido2+ "' ";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 4.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido2 = '"+apellido2+"' ";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 4.2: "+ unaRestriccion);
			}
		}
		if (fechaNacimiento != null && fechaNacimiento.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nac_fecha = '"+fechaNacimiento+"'";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 5.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nac_fecha = '"+fechaNacimiento+ "' ";
				LogFile.log("EpisodioSiapwin: construirRestriccion: WHERE 5.2: "+ unaRestriccion);
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			LogFile.log("EpisodioSiapwin: construirRestriccion: unaRestriccion es null.");
			unaRestriccion = "";
		}
		LogFile.log("EpisodioSiapwin: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion




	/**
	* M�todo que devuelve los episodios 
	*
	*@param 
	*@return vector con episodios
	*/
	public static Vector getEpisodios(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
									throws SQLException, Exception {

		String unaRestriccion = EpisodioSiapwin.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);

		LogFile.log("EpisodioSiapwin: getEpisodios: WHERE-> "+unaRestriccion);

		Vector vEpisodiosSiapwin = EpisodioSiapwin.instancesWithWhere(unaRestriccion);
		LogFile.log("EpisodioSiapwin: getEpisodios: size: "+vEpisodiosSiapwin.size());

		if(vEpisodiosSiapwin == null){
			LogFile.log("EpisodioSiapwin: getEpisodios: Vector NULO ");
			vEpisodiosSiapwin = new Vector();
		}



		return vEpisodiosSiapwin;
	} // END OF getEpisodios



	/**
	* M�todo qu� busca los episodios 
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de los Episodios.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vEpisodio = EpisodioSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EpisodioSiapwin"), unaRestriccion);
		if (vEpisodio == null){
		    LogFile.log("EpisodioSiapwin: instancesWithWhere: No hay Episodios.");
		    vEpisodio = new Vector();
		}
		return vEpisodio;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return EpisodioSiapwin.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return EpisodioSiapwinAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Episodio answer = null;
		Vector instances = EpisodioSiapwin.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EpisodioSiapwin"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return EpisodioSiapwin.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EpisodioSiapwin"));
	}
















	/**
	* M�todo qu� devuelve ve_ep_hora 
	*
	* @return ve_ep_hora
	*/
	public String ve_ep_hora()
	{
		return ve_ep_hora;
	}


	/**
	* M�todo qu� setea ve_ep_hora 
	*
	* @param unVe_ep_hora
	*/
	public void ve_ep_hora(String unVe_ep_hora)
	{
		ve_ep_hora = unVe_ep_hora;
	}



	/**
	* M�todo qu� devuelve ve_ep_servicio 
	*
	* @return ve_ep_servicio
	*/
	public String ve_ep_servicio()
	{
		return ve_ep_servicio;
	}


	/**
	* M�todo qu� setea ve_ep_servicio 
	*
	* @param unVe_ep_servicio
	*/
	public void ve_ep_servicio(String unVe_ep_servicio)
	{
		ve_ep_servicio = unVe_ep_servicio;
	}



	/**
	* M�todo qu� devuelve ve_ep_cond 
	*
	* @return ve_ep_cond
	*/
	public String ve_ep_cond()
	{
		return ve_ep_cond;
	}


	/**
	* M�todo qu� setea ve_ep_cond 
	*
	* @param unVe_ep_cond
	*/
	public void ve_ep_cond(String unVe_ep_cond)
	{
		ve_ep_cond = unVe_ep_cond;
	}


	/**
	* M�todo qu� devuelve ve_ep_estado 
	*
	* @return ve_ep_estado
	*/
	public String ve_ep_estado()
	{
		return ve_ep_estado;
	}


	/**
	* M�todo qu� setea ve_ep_estado 
	*
	* @param unVe_ep_estado
	*/
	public void ve_ep_estado(String unVe_ep_estado)
	{
		ve_ep_estado = unVe_ep_estado;
	}


	/**
	* M�todo qu� devuelve ve_ep_idclient 
	*
	* @return ve_ep_idclient
	*/
	public String ve_ep_idclient()
	{
		return ve_ep_idclient;
	}


	/**
	* M�todo qu� setea ve_ep_idclient 
	*
	* @param unVe_ep_idclient
	*/
	public void ve_ep_idclient(String unVe_ep_idclient)
	{
		ve_ep_idclient = unVe_ep_idclient;
	}











 




	




	
	
	/**
	* M�todo qu� devuelve ve_ep_resolucion 
	*
	* @return ve_ep_resolucion
	*/
	public String ve_ep_resolucion()
	{
		return ve_ep_resolucion;
	}


	/**
	* M�todo qu� setea ve_ep_resolucion 
	*
	* @param unVe_ep_resolucion
	*/
	public void ve_ep_resolucion(String unVe_ep_resolucion)
	{
		ve_ep_resolucion = unVe_ep_resolucion;
	}
	












	





	





















/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}

/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:19:21)
 * @return java.lang.String
 */
public java.lang.String getDiagnostico() {
	if(ve_ep_cond==null)
	return Parametros.sinDatos;
	
	return ve_ep_cond;
}



/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:19:21)
 * @return java.lang.String
 */
public java.lang.String getServicio() {
	if(ve_ep_servicio==null)
	return Parametros.sinDatos;
	
	return ve_ep_servicio;
}




/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * oid method comment.
 */
public java.lang.String oid() {

	return ve_ep_ref;
}/**
 * oid method comment.
 */
public void oid(java.lang.String anOID) {
	
	ve_ep_ref = anOID;

}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}	/**
	* M�todo qu� devuelve ve_cond_pk 
	*
	* @return ve_cond_pk
	*/
public java.lang.String ve_cond_pk() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_cond_pk 
	*
	* @param unVe_cond_pk
	*/
public void ve_cond_pk(java.lang.String unVe_cond_pk) {}	/**
	* M�todo qu� devuelve ve_enc_apert 
	*
	* @return ve_enc_apert
	*/
public java.lang.String ve_enc_apert() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_enc_apert 
	*
	* @param unVe_enc_apert
	*/
public void ve_enc_apert(java.lang.String unVe_enc_apert) {}	/**
	* M�todo qu� devuelve ve_ep_etiq 
	*
	* @return ve_ep_etiq
	*/
public java.lang.String ve_ep_etiq() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_ep_etiq 
	*
	* @param unVe_ep_etiq
	*/
public void ve_ep_etiq(java.lang.String unVe_ep_etiq) {}/**


	/**
	* M�todo qu� devuelve ve_ep_fecha 
	*
	* @return ve_ep_fecha
	*/
	public String ve_ep_fecha()
	{
		return ve_ep_fecha;
	}


	/**
	* M�todo qu� setea ve_ep_fecha 
	*
	* @param unVe_ep_fecha
	*/
	public void ve_ep_fecha(String unVe_ep_fecha)
	{
		ve_ep_fecha = unVe_ep_fecha;
	}



public java.lang.String ve_ep_fecha_clin() {
	return null;
}	/**
	* M�todo qu� setea ve_ep_fecha_clin 
	*
	* @param unVe_ep_fecha_clin
	*/
public void ve_ep_fecha_clin(java.lang.String unVe_ep_fecha_clin) {}/**
 * ve_ep_fechaend method comment.
 */
public java.lang.String ve_ep_fechaend() {
	return null;
}	/**
	* M�todo qu� setea ve_ep_fechaend 
	*
	* @param unVe_ep_fechaend
	*/
public void ve_ep_fechaend(java.lang.String unVe_ep_fechaend) {}	/**
	* M�todo qu� devuelve ve_ep_hora_clin 
	*
	* @return ve_ep_hora_clin
	*/
public java.lang.String ve_ep_hora_clin() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_ep_hora_clin 
	*
	* @param unVe_ep_hora_clin
	*/
public void ve_ep_hora_clin(java.lang.String unVe_ep_hora_clin) {}/**
 * ve_ep_icd_cod method comment.
 */
public java.lang.String ve_ep_icd_cod() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_ep_icd_cod 
	*
	* @param unVe_ep_icd_cod
	*/
public void ve_ep_icd_cod(java.lang.String unVe_ep_icd_cod) {}	/**
	* M�todo abstracto que permite devolver el sistema al cu�l se asocia �sta instancia de
	* Episodio.
	*
	* @return SistemaSalud   Instancia del Sistema al cu�l se asocia el Episodio. Por ejemplo
	*                        Sistema Siapwin � Sistem a Novahis.
	*/
public java.lang.String ve_ep_ref() {
	return ve_ep_ref;
}	/**
	* M�todo qu� setea ve_ep_ref 
	*
	* @param unVe_ep_ref
	*/
public void ve_ep_ref(java.lang.String unVe_ep_ref) {
	
	ve_ep_ref = unVe_ep_ref;
}	/**
	* M�todo qu� devuelve ve_fus_pk 
	*
	* @return ve_fus_pk
	*/
public java.lang.String ve_fus_pk() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_fus_pk 
	*
	* @param unVe_fus_pk
	*/
public void ve_fus_pk(java.lang.String unVe_fus_pk) {}	/**
	* M�todo qu� devuelve ve_sev_pk 
	*
	* @return ve_sev_pk
	*/
public java.lang.String ve_sev_pk() {
	return "s/d.";
}	/**
	* M�todo qu� setea ve_sev_pk 
	*
	* @param unVe_sev_pk
	*/
public void ve_sev_pk(java.lang.String unVe_sev_pk) {}} // END OF CLASS Episodio