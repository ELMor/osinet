package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

public class SistemaNovahis extends SistemaSalud {

  private static SistemaNovahis instance;
  
	/**
	 * Constructor
	 *
	 */
	private SistemaNovahis() {
		super();
	} // END OF Constructor

/**
* M�todo privado que devuelve la �nica Instancia de la Clase SistemaNovahis qu� existe
* para todo el Sistema.
* 
* @return instance   Instancia de la Clase SistemaNovahis.
*/
	public static SistemaNovahis getInstance() {
		if(instance == null)
		{
			instance = new SistemaNovahis();
		}
		return instance;
	}

/**
* M�todo qu� devuelve una Cadena de Caracteres que representa el nombre de la Clase
* a la cu�l pertenece �sta instancia ("SistemaNovahis").
* 
* @return String  Devuelve la Cadena de Caracteres "Sistema Novahis". 
*/
	public String nombreSistema() {
		return new String("SistemaNovahis");   
	} // END OF String nombreSistema()

	public PersistenceConfiguration getConfiguration()
	{
		return new SistemaNovahisConfiguration();
	}
}