package com.ssi.model.salud;

import java.util.Vector;
import com.ssi.persistence.cache.*;
import com.ssi.persistence.model.*;

public class EpisodioSiapwinAccessor extends PersistenceAccessor
{


	public static String className()
	{
		return "com.ssi.model.salud.EpisodioSiapwinAccessor";
	}

	public static PersistenceAccessor getInstance()
	{
	    return AccessorCache.getInstance().get(className());
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EpisodioSiapwinAccessor(aManager);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
	{
	    PersistenceAccessor answer = AccessorCache.getInstance().get(className());
	    if(answer == null)
	    {
			answer = new EpisodioSiapwinAccessor(aManager, aCache);
			AccessorCache.getInstance().put(className(), answer);
		}
		return answer;
	}

	protected EpisodioSiapwinAccessor(PersistenceManager aManager)
	{
		super (aManager);
	}

	protected EpisodioSiapwinAccessor(PersistenceManager aManager, IPersistenceCache aCache)
	{
		super(aManager, aCache);
	}

	public  Vector addPersistentFields( Vector aAnswer )
	{
	    //todos las clases accessors deben agregar este primer campo         
		aAnswer.addElement(new PersistentField("ve_ep_ref", "oid", 1, new DBStringFieldType()));
        aAnswer.addElement(new PersistentField("ve_ep_fecha", "ve_ep_fecha", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_hora", "ve_ep_hora", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_idclient", "ve_ep_idclient", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_servicio", "ve_ep_servicio", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_cond", "ve_ep_cond", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_estado", "ve_ep_estado", 1, new DBStringFieldType()));
		aAnswer.addElement(new PersistentField("ve_ep_resolucion", "ve_ep_resolucion", 1, new DBStringFieldType()));

		return aAnswer;
	
	}
	public Vector collectionSpecs()
	{
		return new Vector();
	}
	public String oidPrefix()
	{
		return "";
	}
	public String persistentClassName()
	{
		return "com.ssi.model.salud.EpisodioSiapwin";
	}
	public String tableName()
	{
		return "ve_episodio";
	}
}