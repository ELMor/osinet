package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;

import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;/**
* Clase que representa a un Episodio registrado por el Sistema Novahis para un
* Paciente determinado.
*/
public class EpisodioNovahis extends EpisodioSalud {

			private String ve_ep_ref        ;// 15 L
			private String ve_ep_fecha      ;// 10 YMD
			private String ve_ep_hora       ;// 12 L
			private String ve_ep_servicio   ;//40 L
			private String ve_ep_cond       ;// 120 L
			private String ve_ep_estado     ;// 6 N
			private String ve_ep_idclient   ;// 15 L
			private String ve_sev_pk        ;// 6 N
			private String ve_fus_pk        ;// 11 N
			private String ve_cond_pk       ;// 12 L
			private String ve_ep_fechaend   ;// 10 YMD
			private String ve_ep_resolucion ;// 120 L
			private String ve_enc_apert     ;// 15 L
			private String ve_ep_icd_cod    ;// 12 L
			private String ve_ep_fecha_clin ;// 10 YMD
			private String ve_ep_hora_clin  ;// 12 L
			private String ve_ep_etiq       ;// 12 L


/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Episodio
* por defecto.
*/
	public EpisodioNovahis(){
		super();
	} // END OF Constructor Episodio



	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return EpisodioNovahis.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaNovahis.getInstance();
	}


	public static String construirRestriccion(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento) {
		String unaRestriccion = null;
		if (rfc!=null && rfc.length() > 0){
			unaRestriccion = " codigo1 = '" +rfc+ "'";
			LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		if (nombre!= null && nombre.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nombre = '"+nombre+ "'";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 2.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nombre = '"+nombre+"'";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 2.2: "+ unaRestriccion);
			}
		}
		if (apellido1 != null && apellido1.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido1 = '"+apellido1+ "' ";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 3.1: "+ unaRestriccion);
			}
			else{
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido1 = '"+apellido1+ "'";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 3.2: "+ unaRestriccion);
			}
		}
		if (apellido2 != null && apellido2.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " apellido2 = '"+apellido2+ "' ";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 4.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" apellido2 = '"+apellido2+"' ";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 4.2: "+ unaRestriccion);
			}
		}
		if (fechaNacimiento != null && fechaNacimiento.length() > 0){
			if(unaRestriccion == null){
				unaRestriccion = " nac_fecha = '"+fechaNacimiento+"'";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 5.1: "+ unaRestriccion);
			}
			else {
				unaRestriccion = unaRestriccion +PacienteSalud.darAnd()+" nac_fecha = '"+fechaNacimiento+ "' ";
				LogFile.log("EpisodioNovahis: construirRestriccion: WHERE 5.2: "+ unaRestriccion);
			}
		}
		if (unaRestriccion!=null ){
			unaRestriccion = " where "+unaRestriccion;
		} else{
			LogFile.log("EpisodioNovahis: construirRestriccion: unaRestriccion es null.");
			unaRestriccion = "";
		}
		LogFile.log("EpisodioNovahis: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion




	/**
	* M�todo que devuelve los episodios 
	*
	*@param 
	*@return vector con episodios
	*/
	public static Vector getEpisodios(String rfc,String nombre,String apellido1,String apellido2, String fechaNacimiento)
									throws SQLException, Exception {

		String unaRestriccion = EpisodioNovahis.construirRestriccion(rfc,nombre,apellido1,apellido2,fechaNacimiento);

		LogFile.log("Episodio: getEpisodios: WHERE-> "+unaRestriccion);

		Vector vEpisodios = EpisodioNovahis.instancesWithWhere(unaRestriccion);
		LogFile.log("Episodio: getEpisodios: size: "+vEpisodios.size());

		if(vEpisodios == null){
			LogFile.log("EpisodioNovahis: getEpisodios: Vector NULO ");
			vEpisodios = new Vector();
		}



		return vEpisodios;
	} // END OF getEpisodios



	/**
	* M�todo qu� busca los episodios 
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de los Episodios.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vEpisodio = EpisodioNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.EpisodioNovahis"), unaRestriccion);
		if (vEpisodio == null){
		    LogFile.log("Episodio: instancesWithWhere: No hay Episodios.");
		    vEpisodio = new Vector();
		}
		return vEpisodio;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		LogFile.log("EpisodioNovahis::getPersistenceManager");
		return EpisodioNovahis.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return EpisodioNovahisAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Episodio answer = null;
		Vector instances = EpisodioNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.EpisodioNovahis"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return EpisodioNovahis.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.EpisodioNovahis"));
	}


	/**
	* M�todo qu� devuelve ve_ep_ref 
	*
	* @return ve_ep_ref
	*/
	public String ve_ep_ref(){
		return ve_ep_ref;
	} // END OF method

	/**
	* M�todo qu� setea ve_ep_ref 
	*
	* @param unVe_ep_ref
	*/
	public void ve_ep_ref(String unVe_ep_ref){
		ve_ep_ref = unVe_ep_ref;
	} // END OF method

/**
*
*/
	public String centroDescripcion(){
		return "";
	}







	/**
	* M�todo qu� devuelve ve_ep_fecha 
	*
	* @return ve_ep_fecha
	*/
	public String ve_ep_fecha()
	{
		return ve_ep_fecha;
	}


	/**
	* M�todo qu� setea ve_ep_fecha 
	*
	* @param unVe_ep_fecha
	*/
	public void ve_ep_fecha(String unVe_ep_fecha)
	{
		ve_ep_fecha = unVe_ep_fecha;
	}


	/**
	* M�todo qu� devuelve ve_ep_hora 
	*
	* @return ve_ep_hora
	*/
	public String ve_ep_hora()
	{
		return ve_ep_hora;
	}


	/**
	* M�todo qu� setea ve_ep_hora 
	*
	* @param unVe_ep_hora
	*/
	public void ve_ep_hora(String unVe_ep_hora)
	{
		ve_ep_hora = unVe_ep_hora;
	}



	/**
	* M�todo qu� devuelve ve_ep_servicio 
	*
	* @return ve_ep_servicio
	*/
	public String ve_ep_servicio()
	{
		return ve_ep_servicio;
	}


	/**
	* M�todo qu� setea ve_ep_servicio 
	*
	* @param unVe_ep_servicio
	*/
	public void ve_ep_servicio(String unVe_ep_servicio)
	{
		ve_ep_servicio = unVe_ep_servicio;
	}



	/**
	* M�todo qu� devuelve ve_ep_cond 
	*
	* @return ve_ep_cond
	*/
	public String ve_ep_cond()
	{
		return ve_ep_cond;
	}


	/**
	* M�todo qu� setea ve_ep_cond 
	*
	* @param unVe_ep_cond
	*/
	public void ve_ep_cond(String unVe_ep_cond)
	{
		ve_ep_cond = unVe_ep_cond;
	}


	/**
	* M�todo qu� devuelve ve_ep_estado 
	*
	* @return ve_ep_estado
	*/
	public String ve_ep_estado()
	{
		return ve_ep_estado;
	}


	/**
	* M�todo qu� setea ve_ep_estado 
	*
	* @param unVe_ep_estado
	*/
	public void ve_ep_estado(String unVe_ep_estado)
	{
		ve_ep_estado = unVe_ep_estado;
	}


	/**
	* M�todo qu� devuelve ve_ep_idclient 
	*
	* @return ve_ep_idclient
	*/
	public String ve_ep_idclient()
	{
		return ve_ep_idclient;
	}


	/**
	* M�todo qu� setea ve_ep_idclient 
	*
	* @param unVe_ep_idclient
	*/
	public void ve_ep_idclient(String unVe_ep_idclient)
	{
		ve_ep_idclient = unVe_ep_idclient;
	}

	/**
	* M�todo qu� devuelve ve_sev_pk 
	*
	* @return ve_sev_pk
	*/
	public String ve_sev_pk()
	{
		return ve_sev_pk;
	}


	/**
	* M�todo qu� setea ve_sev_pk 
	*
	* @param unVe_sev_pk
	*/
	public void ve_sev_pk(String unVe_sev_pk)
	{
		ve_sev_pk = unVe_sev_pk;
	}

	/**
	* M�todo qu� devuelve ve_fus_pk 
	*
	* @return ve_fus_pk
	*/
	public String ve_fus_pk()
	{
		return ve_fus_pk;
	}


	/**
	* M�todo qu� setea ve_fus_pk 
	*
	* @param unVe_fus_pk
	*/
	public void ve_fus_pk(String unVe_fus_pk)
	{
		ve_fus_pk = unVe_fus_pk;
	}

 
	/**
	* M�todo qu� devuelve ve_cond_pk 
	*
	* @return ve_cond_pk
	*/
	public String ve_cond_pk()
	{
		return ve_cond_pk;
	}


	/**
	* M�todo qu� setea ve_cond_pk 
	*
	* @param unVe_cond_pk
	*/
	public void ve_cond_pk(String unVe_cond_pk)
	{
		ve_cond_pk = unVe_cond_pk;
	}
	
	/**
	* M�todo qu� devuelve ve_ep_fechaend 
	*
	* @return ve_ep_fechaend
	*/
	public String ve_ep_fechaend()
	{
		return ve_ep_fechaend;
	}


	/**
	* M�todo qu� setea ve_ep_fechaend 
	*
	* @param unVe_ep_fechaend
	*/
	public void ve_ep_fechaend(String unVe_ep_fechaend)
	{
		ve_ep_fechaend = unVe_ep_fechaend;
	}
	
	
	/**
	* M�todo qu� devuelve ve_ep_resolucion 
	*
	* @return ve_ep_resolucion
	*/
	public String ve_ep_resolucion()
	{
		return ve_ep_resolucion;
	}


	/**
	* M�todo qu� setea ve_ep_resolucion 
	*
	* @param unVe_ep_resolucion
	*/
	public void ve_ep_resolucion(String unVe_ep_resolucion)
	{
		ve_ep_resolucion = unVe_ep_resolucion;
	}
	

	/**
	* M�todo qu� devuelve ve_enc_apert 
	*
	* @return ve_enc_apert
	*/
	public String ve_enc_apert()
	{
		return ve_enc_apert;
	}


	/**
	* M�todo qu� setea ve_enc_apert 
	*
	* @param unVe_enc_apert
	*/
	public void ve_enc_apert(String unVe_enc_apert)
	{
		ve_enc_apert = unVe_enc_apert;
	}


	/**
	* M�todo qu� devuelve ve_ep_icd_cod 
	*
	* @return ve_ep_icd_cod
	*/
	public String ve_ep_icd_cod()
	{
		return ve_ep_icd_cod;
	}


	/**
	* M�todo qu� setea ve_ep_icd_cod 
	*
	* @param unVe_ep_icd_cod
	*/
	public void ve_ep_icd_cod(String unVe_ep_icd_cod)
	{
		ve_ep_icd_cod = unVe_ep_icd_cod;
	}

	
	/**
	* M�todo qu� devuelve ve_ep_fecha_clin 
	*
	* @return ve_ep_fecha_clin
	*/
	public String ve_ep_fecha_clin()
	{
		return ve_ep_fecha_clin;
	}


	/**
	* M�todo qu� setea ve_ep_fecha_clin 
	*
	* @param unVe_ep_fecha_clin
	*/
	public void ve_ep_fecha_clin(String unVe_ep_fecha_clin)
	{
		ve_ep_fecha_clin = unVe_ep_fecha_clin;
	}

	
	/**
	* M�todo qu� devuelve ve_ep_hora_clin 
	*
	* @return ve_ep_hora_clin
	*/
	public String ve_ep_hora_clin()
	{
		return ve_ep_hora_clin;
	}


	/**
	* M�todo qu� setea ve_ep_hora_clin 
	*
	* @param unVe_ep_hora_clin
	*/
	public void ve_ep_hora_clin(String unVe_ep_hora_clin)
	{
		ve_ep_hora_clin = unVe_ep_hora_clin;
	}


	/**
	* M�todo qu� devuelve ve_ep_etiq 
	*
	* @return ve_ep_etiq
	*/
	public String ve_ep_etiq()
	{
		return ve_ep_etiq;
	}


	/**
	* M�todo qu� setea ve_ep_etiq 
	*
	* @param unVe_ep_etiq
	*/
	public void ve_ep_etiq(String unVe_ep_etiq)
	{
		ve_ep_etiq = unVe_ep_etiq;
	}

	public String oid()
	{
		return ve_ep_ref;
	}

	public void oid(String unCodigo)
	{
		ve_ep_ref = unCodigo;
	}

/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:19:16)
 * @return java.lang.String
 */
public java.lang.String getDiagnostico() {
	
	if(diagnostico==null)
	{
		java.sql.Statement stm = null;
		java.sql.ResultSet rs  = null;
	    try
	    {
    	    PersistenceManager manager = SistemaNovahis.getInstance().getPersistenceManager();
    		String query = "select icd_nom	from icd where icd_cod='"+ this.ve_ep_icd_cod()+"'";
    	    //JDBCAnswerResultSet rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
    	    //String desc= rs2.resultSet().getString("icd_nom");
    		DBConnection conexion = manager.getConnection();

    		String desc =null;
    		stm = ((java.sql.Connection)conexion.connection()).createStatement();
    		rs = stm.executeQuery(query);
    		while(rs.next())
     		   diagnostico= rs.getString("icd_nom");
    		  //desc= rs.getString("icd_nom");
    		rs.close();
    		stm.close();
    		manager.returnConnection(conexion); 
    		
	    }	
	    catch(Exception e){ LogFile.log(e); }
	
	 finally{
		   try{
		    rs.close();
    		stm.close();
    		
		   }
		   catch(Exception e){
			LogFile.log("EpisodioNovahis no puede cerrar el ResultSet");
		   }	
	   }	  
    }
    if(diagnostico==null)
      	return Parametros.sinDatos;
	
    return diagnostico;

}



public java.lang.String getServicio() {
	
	if(servicio==null)
	{
		java.sql.Statement stm = null;
		java.sql.ResultSet rs  = null;
	    try
	    {
    	    PersistenceManager manager = SistemaNovahis.getInstance().getPersistenceManager();
    		String query = "select servicio	from servicios where codigo_servicio='"+ this.ve_ep_servicio()+"'";
    		DBConnection conexion = manager.getConnection();

    		String desc =null;
    		stm = ((java.sql.Connection)conexion.connection()).createStatement();
    		rs = stm.executeQuery(query);
    		while(rs.next())
     		   servicio= rs.getString("servicio");
    		
    		rs.close();
    		stm.close();
    		manager.returnConnection(conexion); 
    		
	    }	
	    catch(Exception e){ LogFile.log(e); }
	
	 finally{
		   try{
		    rs.close();
    		stm.close();
    		
		   }
		   catch(Exception e){
			LogFile.log("EpisodioNovahis no puede cerrar el ResultSet");
		   }	
	   }	  
    }
    if(servicio==null)
      	return Parametros.sinDatos;
	
    return servicio;

}






/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}/*   private String ve_ep_ref        ;// 15 L
			private String ve_ep_fecha      ;// 10 YMD
			private String ve_ep_hora       ;// 12 L
			private String ve_ep_servicio   ;//40 L
			private String ve_ep_cond       ;// 120 L
			private String ve_ep_estado     ;// 6 N
			private String ve_ep_idclient   ;// 15 L
			private String ve_sev_pk        ;// 6 N
			private String ve_fus_pk        ;// 11 N
			private String ve_cond_pk       ;// 12 L
			private String ve_ep_fechaend   ;// 10 YMD
			private String ve_ep_resolucion ;// 120 L
			private String ve_enc_apert     ;// 15 L
			private String ve_ep_icd_cod    ;// 12 L
			private String ve_ep_fecha_clin ;// 10 YMD
			private String ve_ep_hora_clin  ;// 12 L
			private String ve_ep_etiq       ;// 12 L
			private ProxyInterface codigo_centro ;   */

} // END OF CLASS Episodio