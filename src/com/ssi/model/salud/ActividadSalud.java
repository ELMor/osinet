package com.ssi.model.salud;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.reflect.InvocationTargetException;
import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;

import com.ssi.util.*;import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;/**
* Clase que representa Avtividades para un Paciente determinado.
*/

public abstract class ActividadSalud extends ReadWritePersistentObject{
	private Paciente unPaciente = null;



	/**
	* M�todo qu� permite setear el Paciente vinculado a �sta instancia de actividad.
	*
	* @param paciente    Instancia de la Clase Paciente.
	*/
	public void paciente(Paciente paciente){
		unPaciente = paciente;
	} // END OF Episodio()

	/**
	* M�todo qu� permite devolver el Paciente vinculado a �sta instancia de actividad.
	*
	* @return unPaciente    Instancia de la Clase Paciente.
	*/
	public Paciente paciente(){
		return unPaciente;
	} // END OF getPaciente()

	/**
	* M�todo qu� permite devolver el nombre del Sistema al cu�l est� asociado �sta instancia
	* de Episodio; para ser usado por ejemplo en una pantalla como dato informativo.
	*
	* @return String   Descripcion expl�cita del nombre del Sistema al que se asocia la actividad.
	*/
	public String nombreSistema(){
		return sistema().nombreSistema();
	} // END OF getNombreSistema()

	/**
	* M�todo abstracto que permite devolver el sistema al cu�l se asocia �sta instancia de
	* actividad.
	*
	* @return SistemaSalud   Instancia del Sistema al cu�l se asocia la actividad. Por ejemplo
	*                        Sistema Siapwin � Sistem a Novahis.
	*/
	public abstract SistemaSalud sistema();

	public static java.util.Vector listaActividades;	
	static java.util.Hashtable listaTipoActividad=null;	
	java.lang.String tipoActividad;
	
/**
 * Insert the method's description here.
 * Creation date: (19/02/01 10:23:20)
 */
public ActividadSalud() {
	
	unPaciente = new Paciente();
	
	}/**
 * Insert the method's description here.
 * Creation date: (19/02/01 11:35:07)
 * @return java.lang.String
 */
public String descripcion() {
	return ve_ac_descri();
}/**
 * Insert the method's description here.
 * Creation date: (19/02/01 11:33:44)
 */
public String fechayHoraInicio(){
		
		String campoFecha=null;
  
		   if(ve_ac_fecha()!=null){
			 java.sql.Date date = java.sql.Date.valueOf(ve_ac_fecha());
			 java.text.DateFormat myformat = new java.text.SimpleDateFormat("dd/MM/yyyy");  
			 campoFecha = myformat.format(date) +" "+ ve_ac_hora();
			}
			else{ 
			 campoFecha = Parametros.sinDatos;
			}  
		
		return campoFecha;
	}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:32:32)
 * @return java.lang.String
 */
public abstract java.lang.String getTipoActividad();/**
 * Insert the method's description here.
 * Creation date: (15/03/01 14:56:32)
 * @return java.lang.String
 * @param idActividad java.lang.String
 */
public static String getTipoActividad(String idActividad) {

	if(listaTipoActividad == null){
	
	  listaTipoActividad = new Hashtable();
	  
	  JDBCAnswerResultSet rs2 = null;
	  
	  try{
	    PersistenceManager manager = SistemaSiapwin.getInstance().getPersistenceManager();
	    String query  =  "SELECT tipo_actividad_pk,tipo_act_desc FROM tipo_actividad";
	    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
	    ActividadSalud.setearTiposDeActividades(rs2.resultSet());
	    LogFile.log("Finaliz� de setear los tipos de actividades");  
	   }	
	   
	   catch(Exception e){ LogFile.log(e); }
	   
	   finally{
		   try{
		    rs2.close();
		   }
		   catch(Exception e){
			LogFile.log("ActividadSalud no puede cerrar el ResultSet");
		   }	
	   }	   
	   
	}
	
 	if(listaTipoActividad.containsKey(idActividad)){
	 return (String)listaTipoActividad.get(idActividad);
	} 
	
	return Parametros.sinDatos;

}	public abstract String oid();/**
 * Insert the method's description here.
 * Creation date: (15/03/01 13:18:00)
 */
public static void setearTiposDeActividades(java.sql.ResultSet rs) {
	
	
	try{     

	    String clave= rs.getString("tipo_actividad_pk");
		String descripcion = rs.getString("tipo_act_desc");
		listaTipoActividad.put((Object)clave,(Object)descripcion);
		LogFile.log("Leyendo tipo de actividad: "+clave+", "+descripcion); 
	    
	    while(rs.next()){
		  clave= rs.getString("tipo_actividad_pk");
		  descripcion = rs.getString("tipo_act_desc");
		  listaTipoActividad.put((Object)clave,(Object)descripcion);
		  LogFile.log("Leyendo tipo de actividad: "+clave+", "+descripcion); 
	    }  
	}
	catch(Exception e){
	    LogFile.log("Excepcion en ActividadSalud::setearTiposDeActividades()");   
		LogFile.log(e);
	}    
	  
	
 }         /**
 * Insert the method's description here.
 * Creation date: (15/03/01 12:32:32)
 * @param newTipoActividad java.lang.String
 */
public abstract void setTipoActividad(java.lang.String newTipoActividad);	/**
	* M�todo qu� devuelve ve_ac_coste
	*
	* @return ve_ac_coste
	*/
	public abstract String ve_ac_coste();	/**
	* M�todo qu� devuelve ve_ac_descri
	*
	* @return ve_ac_descri
	*/
	public abstract String ve_ac_descri();	/**
	* M�todo qu� devuelve ve_ac_estado
	*
	* @return ve_ac_estado
	*/
	public abstract String ve_ac_estado();	/**
	* M�todo qu� devuelve ve_ac_fecha
	*
	* @return ve_ac_fecha
	*/
	public abstract String ve_ac_fecha();	/**
	* M�todo qu� devuelve ve_ac_hora
	*
	* @return ve_ac_hora
	*/
	public abstract String ve_ac_hora();	/**
	* M�todo qu� devuelve ve_ac_ref
	*
	* @return ve_ac_ref
	*/
	public abstract String ve_ac_ref();	/**
	* M�todo qu� devuelve ve_ac_servicio
	*
	* @return ve_ac_servicio
	*/
	public abstract String ve_ac_servicio();	/**
	* M�todo qu� devuelve ve_ac_tipo
	*
	* @return ve_ac_tipo
	*/
	public abstract String ve_ac_tipo();	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
	public abstract String ve_en_ref();}// END OF Class ActividaSalud