package com.ssi.model.salud;

import com.ssi.model.salud.*;
import java.sql.*;
import java.util.*;
import com.ssi.util.*;
import java.util.Vector;
import java.lang.reflect.*;
import com.ssi.bean.salud.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import com.ssi.persistence.cache.*;



/**
* Clase que representa a un Encuentro.
*/

public class Encuentro extends EncuentroSalud {

			
			
			private String ve_en_ref      ;// 15 L
			private String ve_ep_ref      ;// 15 L
			private String ve_en_fecha    ;// 10 YMD
			private String ve_en_hora     ;// 12 L
			private String ve_en_servicio ;// 40 L
			private String ve_en_motivo   ;// 120 L
			private String ve_en_estado   ;// 6 N
			private String ve_en_tipo     ;// 6 N
			private String ve_en_idclient ;// 15 L
			private String ve_en_icd_cod  ;// 12 L
			
			


/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase Encuentro
* por defecto.
*/
	public Encuentro(){
		super();
	} // END OF Constructor Encuentro



	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return Encuentro.getPersistenceManager();
	}

	public SistemaSalud sistema(){
		return SistemaNovahis.getInstance();
	}


	public static String construirRestriccion(String idEncuentro) {
		String unaRestriccion = null;
		if (idEncuentro!=null && idEncuentro.length() > 0){
			unaRestriccion = " WHERE ve_ep_ref = '" +idEncuentro+ "'";
			LogFile.log("Encuentro: construirRestriccion: WHERE 1: "+ unaRestriccion);
		}
		LogFile.log("Encuentro: construirRestriccion: FIN");
		return unaRestriccion;
	} // END OF construirRestriccion






	/**
	* M�todo qu� busca los Encuentro
	* Este m�todo devuelve el Vector Generado.
	*
	* @param unaRestriccion:Cadena de Caracteres qu� representa la restriccion (filtro) de la SQL-Query que busca los datos de los Episodios.
	* @return  Vector que contiene los datos de los Encuentro.
	*/
	public static Vector instancesWithWhere(String unaRestriccion) throws InstantiationException, IllegalAccessException,
										ClassNotFoundException, InvocationTargetException,
									   NoSuchMethodException,SQLException
	{
		Vector vEncuentro = Encuentro.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Encuentro"), unaRestriccion);
		if (vEncuentro == null){
		    LogFile.log("Encuentro: instancesWithWhere: No hay Encuentro.");
		    vEncuentro = new Vector();
		}
		return vEncuentro;
	} // END OF instancesWithWhere



	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return Encuentro.getPersistenceAccessor().persistenceManager();
	}


	public static PersistenceAccessor getPersistenceAccessor()
	{
		return EncuentroAccessor.getInstance();
	}


	public static Vector findAll(String unaRestriccion)
	{
	  try
	  {
	    Encuentro answer = null;
		Vector instances = Encuentro.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Encuentro"), unaRestriccion);

	    return instances;

	  }
	  catch (Exception ex)
	  {
		return new Vector() ;
	  }

	}


	public static Vector instances()
		throws InstantiationException,
			   IllegalAccessException,
			   ClassNotFoundException,
			   InvocationTargetException,
			   NoSuchMethodException,
			   SQLException
	{
		return Encuentro.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Encuentro"));
	}


	/**
	* M�todo qu� devuelve el Identificador �nico del Encuentro (que refleja el valor del
	* campo ve_en_ref) en formato de Cadena de Caracteres.
	*
	* @return ve_en_ref
	*/
	public String idEncuentro(){
		return ve_en_ref;
	}

	/**
	* M�todo qu� setea el Identificador �nico del Encuentro (que refleja el valor del
	* campo ve_en_ref); para ello recibe una Cadena de Caracteres que se pasa como 
	* parametro.
	*
	* @param unVe_ep_ref
	*/
	public void idEncuentro(String unIdEncuentro){
		ve_en_ref = unIdEncuentro;
	}

	/**
	* M�todo qu� devuelve la fecha y hora de inicio del Encuentro como una cadena de
	* caracteres.
	*
	* @return ve_en_fecha
	*/
	public String fechayHoraInicio(){
		String fechayHora = null;
		fechayHora = this.fechaInicio();
		fechayHora = fechayHora +" "+ this.horaInicio();
		return fechayHora;
	}

	/**
	* M�todo qu� setea la Fecha de Inicio del Encuentro (valor reflejado en la BD por
	* el campo ve_en_fecha).
	*
	* @param unafechaInicio
	*/
	public void fechaInicio(String unafechaInicio) {
		ve_en_fecha = unafechaInicio;
	}

	/**
	* M�todo qu� devuelve la Fecha de Inicio del Encuentro (valor reflejado en la BD por
	* el campo ve_en_fecha).
	*
	* @return ve_en_fecha
	*/
	public String fechaInicio() {
		return ve_en_fecha;
	}

	/**
	* M�todo qu� devuelve la Hora de Inicio del Encuentro (valor reflejado en la BD por
	* el campo ve_en_hora).
	*
	* @return ve_en_hora
	*/
	public String horaInicio(){
		return ve_en_hora;
	}

	/**
	* M�todo qu� setea la Hora de Inicio del Encuentro (valor reflejado en la BD por
	* el campo ve_en_hora).
	*
	* @param unahoraInicio
	*/
	public void horaInicio(String unahoraInicio)
	{
		ve_en_hora = unahoraInicio;
	}

	/**
	* M�todo qu� devuelve ve_ep_ref
	*
	* @return ve_ep_ref
	*/
	public String ve_ep_ref()
	{
		return ve_ep_ref;
	}


	/**
	* M�todo qu� setea ve_ep_ref
	*
	* @param unVe_ep_ref
	*/
	public void ve_ep_ref(String unVe_ep_ref)
	{
		ve_ep_ref = unVe_ep_ref;
	}

	/**
	* M�todo qu� devuelve la descripcion del Servicio propio del Encuentro. 
	*
	* @return String    Cadena de Caracteres que indica que Servicio est� asociado
	*                   al Encuentro.
	*/
	public String servicio(){
		String respuesta =	this.ve_en_servicio();
		if(respuesta== null){
		   respuesta = new String();
		   respuesta = "Sin Dato";
		}
		return respuesta;
	} // END OF servicio()

	/**
	* M�todo qu� devuelve ve_en_servicio
	*
	* @return ve_en_servicio
	*/
	public String ve_en_servicio()
	{
		return ve_en_servicio;
	}


	/**
	* M�todo qu� setea ve_en_servicio
	*
	* @param unVe_en_servicio
	*/
	public void ve_en_servicio(String unVe_en_servicio)
	{
		ve_en_servicio = unVe_en_servicio;
	}

	/**
	* M�todo qu� devuelve una Cadena de Caracteres que representa el Motivo del 
	* Encuentro. Si no existiese este dato en la Base de Datos; devuelve la cadena
	* 'SIN MOTIVO'.
	* 
	*@return String   Cadena de Caracteres que representa el motivo del Encuentro.
	*/
	public String motivo(){
		String motivo = this.ve_en_motivo();
		if(motivo == null){
			motivo = new String();
			motivo = "Sin Motivo";
		}
		return motivo;
	} // END OF motivo


	/**
	* M�todo qu� devuelve ve_en_motivo
	*
	* @return ve_en_motivo
	*/
	public String ve_en_motivo()
	{
		return ve_en_motivo;
	}


	/**
	* M�todo qu� setea ve_en_motivo
	*
	* @param unVe_en_motivo
	*/
	public void ve_en_motivo(String unVe_en_motivo)
	{
		ve_en_motivo = unVe_en_motivo;
	}

	/**
	* M�todo qu� devuelve el Estado de una Encuentro.
	*
	* @return String    Cadena de Caracteres que representa el estado en que se
	*                   encuentra el Encuentro.
	*/
	public String estado(){
		String respuesta = this.ve_en_estado();
		if (respuesta == null){
			respuesta = new String();
			respuesta = "Sin Dato";
		}
		return respuesta;
	} // END OF method estado

	/**
	* M�todo qu� devuelve ve_en_estado
	*
	* @return ve_en_estado
	*/
	public String ve_en_estado()
	{
		return ve_en_estado;
	}


	/**
	* M�todo qu� setea ve_en_estado
	*
	* @param unVe_en_estado
	*/
	public void ve_en_estado(String unVe_en_estado)
	{
		ve_en_estado = unVe_en_estado;
	}



	/**
	* M�todo qu� devuelve ve_en_idclient
	*
	* @return ve_en_idclient
	*/
	public String ve_en_idclient()
	{
		return ve_en_idclient;
	}


	/**
	* M�todo qu� setea ve_en_idclient
	*
	* @param unVe_en_idclient
	*/
	public void ve_en_idclient(String unVe_en_idclient)
	{
		ve_en_idclient = unVe_en_idclient;
	}


	/**
	* M�todo qu� devuelve el Tipo de Encuentro.
	*
	* @return String    Cadena de Caracteres que representa el tipo de Encuentro.
	*/
	public String tipo(){
		String tipo = this.ve_en_tipo();
		if (tipo==null){
			tipo = new String();
			tipo = "No se especific� Tipo";
		}
		return tipo;
	} // END OF tipo

	/**
	* M�todo qu� devuelve ve_en_tipo
	*
	* @return ve_en_tipo
	*/
	public String ve_en_tipo()
	{
		return ve_en_tipo;
	}


	/**
	* M�todo qu� setea ve_en_tipo
	*
	* @param unVe_en_tipo
	*/
	public void ve_en_tipo(String unVe_en_tipo)
	{
		ve_en_tipo = unVe_en_tipo;
	}


	/**
	* M�todo que devuelve el C�digo de Diagn�stico del Encuentro. Si este dato
	* no existiese en la Base de Datos; devuelve la Cadena 'Sin Codigo'.
	*
	* @return String   Cadena que representa el C�digo del Diagn�stico del Encuentro.
	*/
	public String codigoDiagnostico(){
		String codigo = this.ve_en_icd_cod();
		if(codigo == null){
			codigo = new String();
			codigo = "Sin Codigo";
		}
		return codigo;
	} // END OF method codigoDiagnostico()


	/**
	* M�todo qu� devuelve ve_en_icd_cod
	*
	* @return ve_en_icd_cod
	*/
	public String ve_en_icd_cod()
	{
		return ve_en_icd_cod;
	}


	/**
	* M�todo qu� setea ve_en_icd_cod
	*
	* @param unVe_en_icd_cod
	*/
	public void ve_en_icd_cod(String unVe_en_icd_cod)
	{
		ve_en_icd_cod = unVe_en_icd_cod;
	}



	public String oid()
	{
		return ve_en_ref;
	}

	public void oid(String unCodigo)
	{
		ve_en_ref = unCodigo;
	}





/**
 * delete method comment.
 */
public void delete() throws java.lang.NoSuchMethodException, java.sql.SQLException, java.lang.ClassNotFoundException {}
/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:14:39)
 * @return java.lang.String
 */
public java.lang.String getDiagnostico() {
	return null;
}	

/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:14:39)
 * @return java.lang.String
 */


public java.lang.String getServicio() {
	return null;
}	



/**
	* M�todo que devuelve los Encuentro
	*
	*@param
	*@return vector con Encuentro
	*/
	public static Vector getEncuentros(String idEpisodio, String sistema)
									throws SQLException {


	 Vector vEncuentrosSiapwin = new Vector();
 	 Vector vEncuentrosNovahis = new Vector();

	 try{
		    if(sistema.equals("SistemaNovahis")) {
			   LogFile.log("Encuentro: getEncuentro: es Novahis");
	 			String unaRestriccion = " WHERE ve_ep_ref = '" +idEpisodio+ "'";
				vEncuentrosNovahis = EncuentroNovahis.instancesWithWhere(unaRestriccion);
			 }
			 else
			 {
				LogFile.log("Encuentro: getEncuentro: es Siapwin");
				String unaRestriccion = " WHERE ve_ep_ref='"+idEpisodio+ "'";
				vEncuentrosSiapwin = EncuentroSiapwin.instancesWithWhere(unaRestriccion);
			 }
		 }
		 catch(Exception e){
		   
	         LogFile.log(e);
	     }

	 
	 	 LogFile.log("Encuentros: getEncuentros Novahis: size: " + vEncuentrosNovahis.size());
		 LogFile.log("Encuentros: getEncuentros Siapwin: size: " + vEncuentrosSiapwin.size());
	   	   
	     return unificarEncuentros(vEncuentrosNovahis,vEncuentrosSiapwin);

	} // END OF getEpisodios


/*
	  	Vector vEncuentro = Encuentro.instancesWithWhere(unaRestriccion);
		LogFile.log("Encuentro: getEncuentro: size: "+vEncuentro.size());
		if(vEncuentro == null){
			LogFile.log("Encuentro: getEncuentro: Vector NULO ");
			vEncuentro = new Vector();
		}
		return vEncuentro;
	} // END OF getEncuentro */	/**
	* M�todo que devuelve los Encuentros
	*
	*@param
	*@return vector con Encuentro
	*/
	public static Vector getEncuentros(Vector pacientes) throws SQLException, Exception {

/*
	Vector vEncuentrosSiapwin = new Vector();
 	Vector vEncuentrosNovahis = new Vector();

	 try{
		 
	    for(int i = 0; i < pacientes.size(); i++){
		    
			if(pacientes.elementAt(i) instanceof PacienteNovahis){
				
				LogFile.log("Encuentro: getEncuentros: es Novahis");
				String unaRestriccion = " WHERE ve_en_idclient='" + ((PacienteNovahis)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEncuentrosNovahis = EncuentroNovahis.instancesWithWhere(unaRestriccion);
			}
			else
			{
				LogFile.log("Episodio: getEpisodios: es Siapwin");
				Vector vEpisodios = null;    
	            try{
		         vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
	            }
				catch(Exception e){ LogFile.log(e);}
	   
				Enumeration e = vEpisodios.elements();
	            EpisodioSalud episodio = null;
	            String sistema = null;
	            String idEpisodio = null;
				StringBuffer restriccionSiapwin = new StringBuffer(" WHERE ");
				int flagSiapwin = 0;
	  
	            while(e.hasMoreElements()){
	             episodio = (EpisodioSalud)e.nextElement();
	             idEpisodio = episodio.idEpisodio();
	             sistema = episodio.sistema().nombreSistema();
	             if(sistema.equals("Sistema Siapwin")){
	               if(flagSiapwin==0)
	                restriccionSiapwin.append("ve_ep_ref = '" +idEpisodio+ "'");
	               else
	                restriccionSiapwin.append(" OR ve_ep_ref = '" +idEpisodio+ "'");
	              flagSiapwin++; 
	             }	 
	            }
				//Problema: En ve_encuentro de Siapwin no hay  idCliente
				//String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteSiapwin)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEpisodiosSiapwin = EpisodioSiapwin.instancesWithWhere(restriccionSiapwin);
			  }
		    }
	 	 LogFile.log("Encuentro: getEncuentros(Vector) Novahis: size: " + vEncuentrosNovahis.size());
		 LogFile.log("Encuentro: getEncuentros(Vector) Siapwin: size: " + vEncuentrosSiapwin.size());
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarEncuentros(vEncuentrosNovahis,vEncuentrosSiapwin);	

	} // END OF getEpisodios		
		
 /*
	 Vector vEncuentrosSiapwin = new Vector();
 	 Vector vEncuentrosNovahis = new Vector();

	 try{
		 
	         
			 if(unaConeccion.getSingleParamValue("sistema").equals("Sistema Novahis")){
				LogFile.log("Encuentro: getEncuentro: es Novahis");
				String unaRestriccion = " WHERE ve_ep_ref = '" +idEncuentro+ "'";
				vEncuentrosNovahis = EncuentrosNovahis.instancesWithWhere(unaRestriccion);
			 }
			 else
			 {
				LogFile.log("Encuentro: getEncuentro: es Siapwin");
				String unaRestriccion = " WHERE ve_ep_ref='"+idEncuentro+ "'";
				vEncuentrosSiapwin = EncuentrosSiapwin.instancesWithWhere(unaRestriccion);
			 }
		 }

	 	 LogFile.log("Encuentros: getEncuentros Novahis: size: " + vEncuentrosNovahis.size());
		 LogFile.log("Encuentros: getEncuentros Siapwin: size: " + vEncuentrosSiapwin.size());
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarEncuentros(vEncuentrosNovahis,vEncuentrosSiapwin);*/
	return null;
	} // END OF getEpisodios */

/**
 * Insert the method's description here.
 * Creation date: (20/02/01 16:29:58)
 * @return java.util.Vector
 * @param pacientes java.util.Vector
 * @param episodios java.util.Vector
 */
public static Vector getEncuentros(Vector pacientes, Vector vEpisodios) throws SQLException, Exception{

	Vector vEncuentrosSiapwin = new Vector();
 	Vector vEncuentrosNovahis = new Vector();

	 try{
		 
	    for(int i = 0; i < pacientes.size(); i++){
		    
			if(pacientes.elementAt(i) instanceof PacienteNovahis){
				
				LogFile.log("Encuentro: getEncuentros: es Novahis");
				String unaRestriccion = " WHERE ve_en_idclient='" + ((PacienteNovahis)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEncuentrosNovahis = EncuentroNovahis.instancesWithWhere(unaRestriccion);
			}
			else
			{
				LogFile.log("Encuentro: getEncuentros: es Siapwin");
				 
				
				Enumeration e = vEpisodios.elements();
	            EpisodioSalud episodio = null;
	            String sistema = null;
	            String idEpisodio = null;
				StringBuffer restriccionSiapwin = new StringBuffer(" WHERE ");
				int flagSiapwin = 0;
	  
	            while(e.hasMoreElements()){
	             episodio = (EpisodioSalud)e.nextElement();
	             idEpisodio = episodio.ve_ep_ref();
	             sistema = episodio.sistema().nombreSistema();
	             if(sistema.equals("SistemaSiapwin")){
	               if(flagSiapwin==0)
	                restriccionSiapwin.append("ve_ep_ref = '" +idEpisodio+ "'");
	               else
	                restriccionSiapwin.append(" OR ve_ep_ref = '" +idEpisodio+ "'");
	              flagSiapwin++; 
	             }	 
	            }
				//Problema: En ve_encuentro de Siapwin no hay  idCliente
				//String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteSiapwin)pacientes.elementAt(i)).codigoCliente()+ "'";
				vEncuentrosSiapwin = EncuentroSiapwin.instancesWithWhere(restriccionSiapwin.toString());
			  }
		    }
	 	 LogFile.log("Encuentro: getEncuentros(Vector) Novahis: size: " + vEncuentrosNovahis.size());
		 LogFile.log("Encuentro: getEncuentros(Vector) Siapwin: size: " + vEncuentrosSiapwin.size());
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarEncuentros(vEncuentrosNovahis,vEncuentrosSiapwin);	

	} // END OF getEpisodios		
		
 /*
	 Vector vEncuentrosSiapwin = new Vector();
 	 Vector vEncuentrosNovahis = new Vector();

	 try{
		 
	         
			 if(unaConeccion.getSingleParamValue("sistema").equals("Sistema Novahis")){
				LogFile.log("Encuentro: getEncuentro: es Novahis");
				String unaRestriccion = " WHERE ve_ep_ref = '" +idEncuentro+ "'";
				vEncuentrosNovahis = EncuentrosNovahis.instancesWithWhere(unaRestriccion);
			 }
			 else
			 {
				LogFile.log("Encuentro: getEncuentro: es Siapwin");
				String unaRestriccion = " WHERE ve_ep_ref='"+idEncuentro+ "'";
				vEncuentrosSiapwin = EncuentrosSiapwin.instancesWithWhere(unaRestriccion);
			 }
		 }

	 	 LogFile.log("Encuentros: getEncuentros Novahis: size: " + vEncuentrosNovahis.size());
		 LogFile.log("Encuentros: getEncuentros Siapwin: size: " + vEncuentrosSiapwin.size());
	   }
	   catch(Exception e){
		   LogFile.log(e);
	   }	   
		
	   return unificarEncuentros(vEncuentrosNovahis,vEncuentrosSiapwin);

	} // END OF getEpisodios */


/**
 * isPersistent method comment.
 */
public boolean isPersistent() {
	return false;
}/**
 * save method comment.
 */
public void save() throws java.lang.InstantiationException, java.lang.ClassNotFoundException, java.lang.reflect.InvocationTargetException, java.lang.IllegalAccessException, java.lang.NoSuchMethodException, java.sql.SQLException {}/**
 * Insert the method's description here.
 * Creation date: (15/03/01 16:14:39)
 * @param newDiagnostico java.lang.String
 */
public void setDiagnostico(java.lang.String newDiagnostico) {}/**
 * Insert the method's description here.
 * Creation date: (16/02/01 17:07:56)
 */

 public static Vector unificarEncuentros(Vector v1, Vector v2) {
	
	Enumeration e = v2.elements();

	while(e.hasMoreElements())
		v1.addElement(e.nextElement());
		
	return v1;
}
	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
public java.lang.String ve_en_ref() {
	return null;
}} // END OF CLASS Encuentro