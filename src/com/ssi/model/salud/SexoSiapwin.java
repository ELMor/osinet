package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;
/**
* Clase que representa a un Episodio registrado por el Sistema Siapwin para un
* Paciente determinado.
*/

public class SexoSiapwin extends SexoSalud {

/**
* M�todo Constructor qu� Crea e Inicializa una Instancia de la Clase SexoSiapwin con Valores
* por defecto.
*/
	public SexoSiapwin(){
		super();
	} // END OF Constructor SexoSiapwin
	
	public PersistenceManager persistenceManager()
		throws ClassNotFoundException
	{
	    return SexoSiapwin.getPersistenceManager();
	}
	
	public static PersistenceManager getPersistenceManager()
		throws ClassNotFoundException
	{
		//Answers the persistence manager for the receiver
		return SexoSiapwin.getPersistenceAccessor().persistenceManager();
	}

	
	public static PersistenceAccessor getPersistenceAccessor()
	{
		return SexoSiapwinAccessor.getInstance();
	}	
}