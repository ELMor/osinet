package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.model.salud.*;
import com.ssi.util.*;
/**
* Clase que representa a un Centro de Atenci�n registrado por el Sistema.
*/
public abstract class CentroSalud extends ReadPersistentObject {
	private String codigo ;
	private String descripcion ;

	public String oid(){
		return codigo;
	}

	public void oid(String anOID){
		this.codigo = anOID;
	}

	public String descripcion(){
		LogFile.log("CentroSalud: descripcion: "+descripcion);
		return descripcion;
	}

	public void descripcion(String aDescripcion){
		this.descripcion = aDescripcion;
	}
} //END OF Class CentroSalud