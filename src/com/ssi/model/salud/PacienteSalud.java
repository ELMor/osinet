package com.ssi.model.salud;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.reflect.InvocationTargetException;
import com.ssi.persistence.model.*;
import com.ssi.util.*;


public abstract class PacienteSalud extends ReadWritePersistentObject {
	public abstract String sexoDescripcion();

  
   public static String darAnd(){
		return " AND ";
	} // END OF darAnd()
	
   public abstract String nombre();   

   public abstract String apellido1();   
   
   public abstract String apellido2();   

   public abstract GregorianCalendar fechaNacimiento();   
   
   public abstract String RFC();   
   
   public abstract String codigoCliente();   
   
   public abstract String telefono1();   
   
   //public abstract String codigo_sexo();
   
   public abstract String nombreSistema();   
   
   String formatFecha(Calendar fecha)
	{
		int aa = fecha.get(Calendar.YEAR);
		int mm = fecha.get(Calendar.MONTH) + 1;
		int dd = fecha.get(Calendar.DAY_OF_MONTH);
		String ff = dd + "/" + mm + "/" + aa;
		return ff;
	}
   
	public String fechaNacimientoString(Calendar nac_fecha)
	{

		if (nac_fecha == null){String nac_fechaString = Parametros.sinDatos;
		return nac_fechaString;
		}
		else { String nac_fechaString = formatFecha(nac_fecha);
		return nac_fechaString;}
		

	}
   
   
} //END OF class abstract PacienteSalud