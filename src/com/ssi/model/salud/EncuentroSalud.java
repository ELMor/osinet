package com.ssi.model.salud;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import com.ssi.model.salud.*;
import com.ssi.persistence.model.*;
import java.lang.reflect.InvocationTargetException;

/**
* Clase que representa a un Episodio para un Paciente determinado.
*/

public abstract class EncuentroSalud extends ReadWritePersistentObject{

	private Paciente unPaciente = null;

	public abstract void fechaInicio(String unaFecha);
	public abstract String fechaInicio();
	public abstract void horaInicio(String unahora);
	public abstract String horaInicio();
	public abstract String fechayHoraInicio();


	/**
	* M�todo qu� permite setear el Paciente vinculado a �sta instancia de encuentro.
	*
	* @param paciente    Instancia de la Clase Paciente.
	*/
	public void paciente(Paciente paciente){
		unPaciente = paciente;
	} // END OF Episodio()

	/**
	* M�todo qu� permite devolver el Paciente vinculado a �sta instancia de encuentro.
	*
	* @return unPaciente    Instancia de la Clase Paciente.
	*/
	public Paciente paciente(){
		return unPaciente;
	} // END OF getPaciente()

	/**
	* M�todo qu� permite devolver el nombre del Sistema al cu�l est� asociado �sta instancia
	* de Episodio; para ser usado por ejemplo en una pantalla como dato informativo.
	*
	* @return String   Descripcion expl�cita del nombre del Sistema al que se asocia el episodio.
	*/
	public String nombreSistema(){
		return sistema().nombreSistema();
	} // END OF getNombreSistema()

	/**
	* M�todo abstracto que permite devolver el sistema al cu�l se asocia �sta instancia de
	* encuentro.
	*
	* @return SistemaSalud   Instancia del Sistema al cu�l se asocia el encuentro. Por ejemplo
	*                        Sistema Siapwin � Sistem a Novahis.
	*/
	public abstract SistemaSalud sistema();

/**
 * Insert the method's description here.
 * Creation date: (16/02/01 17:38:20)
 */
public EncuentroSalud() {
	
   unPaciente = new Paciente();
	 
 } /**
 * Insert the method's description here.
 * Creation date: (19/02/01 09:17:18)
 * @return java.lang.String
 */
public abstract String idEncuentro();
	abstract public String oid();
		/**
	* M�todo qu� devuelve ve_en_estado
	*
	* @return ve_en_estado
	*/
	abstract public String ve_en_estado();	/**
	* M�todo qu� devuelve ve_en_icd_cod
	*
	* @return ve_en_icd_cod
	*/
	abstract public String ve_en_icd_cod();	/**
	* M�todo qu� devuelve ve_en_idclient
	*
	* @return ve_en_idclient
	*/
	abstract public String ve_en_idclient();	/**
	* M�todo qu� devuelve ve_en_motivo
	*
	* @return ve_en_motivo
	*/
	abstract public String ve_en_motivo();	/**
	* M�todo qu� devuelve ve_en_ref
	*
	* @return ve_en_ref
	*/
	abstract public String ve_en_ref();	/**
	* M�todo qu� devuelve ve_en_servicio
	*
	* @return ve_en_servicio
	*/
	abstract public String ve_en_servicio();	/**
	* M�todo qu� devuelve ve_en_tipo
	*
	* @return ve_en_tipo
	*/
	abstract public String ve_en_tipo();	/**
	* M�todo qu� devuelve ve_ep_ref
	*
	* @return ve_ep_ref
	*/
	abstract public String ve_ep_ref();}// END OF Class EpisodioSalud