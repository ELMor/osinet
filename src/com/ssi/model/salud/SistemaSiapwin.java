package com.ssi.model.salud;

import com.ssi.persistence.model.*;
import com.ssi.persistence.model.salud.*;

public class SistemaSiapwin extends SistemaSalud {


	private static SistemaSiapwin instance;
	
	public static SistemaSiapwin getInstance()
	{
		if(instance == null)
		{
			instance = new SistemaSiapwin();
		}
		return instance;
	}
	
	/**
	 * Constructor
	 *
	 */
	private SistemaSiapwin() {
		super();
	}

/**
* M�todo qu� devuelve una Cadena de Caracteres que representa el nombre de la Clase
* a la cu�l pertenece �sta instancia ("Sistema Siapwin").
* 
* @return String  Devuelve la Cadena de Caracteres "Sistema Novahis". 
*/
	public String nombreSistema() {
		return new String("SistemaSiapwin");   
	} // END OF String nombreSistema()

	public PersistenceConfiguration getConfiguration()
	{
		return new SistemaSiapwinConfiguration();
	}

}