package com.ssi.http;

import java.io.*;
import java.util.*;
import com.ssi.util.*;
import javax.servlet.*;
import javax.servlet.http.*;



/**
* Clase qu� representa una Conecci�n Http, incluyendo el Request, el Response y la Session
*
*/

public abstract class ConeccionHTTP

{
	public void putObjectInSession(String name, Object obj)
	{
		this.getSession().putValue(name, obj);
	}
	/**
	 * Deprecated. Should use getObjectFromSession().
	 */
	public Object getFromSession(String name) throws Exception
	{
		return this.getObjectFromSession(name);
	}
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HttpSession session;

/**
* M�todo Constructor qu� crea una instancia de la Clase
*/
	public ConeccionHTTP(HttpServletRequest esteRequest, HttpServletResponse esteResponse)
				throws Exception
	{
		this.setRequest(esteRequest);
		this.setResponse(esteResponse);
		this.initializeSesison();
	} //fin contructor ConeccionHTTP()


/**
* M�todo qu� inicializa la Session.
* Si no hab�a una session anteriormente, se devuelve null.
*/
	public void initializeSesison() 
	 throws Exception
	{
		HttpSession sess = this.getRequest().getSession(false);
		if (sess==null)
		{
			if (this.requestEstaVacio()) 
			{
				sess = this.getRequest().getSession(true);
			}
			else
			{
				throw new Exception("Su session expir� o su navegador no est� aceptando cookies.");
			}
		}
		this.setSession(sess);
	}


/**
* M�todo qu� retorna la Session.
* Si no hab�a una session anteriormente, se devuelve null.
*/
	public HttpSession getSession() {
		return this.session;
	} //fin metodo getSession()


	public HttpServletRequest getRequest()
	{
		return request;
	} //fin metodo getRequest()

	public HttpServletResponse getResponse()
	{
		return response;
	} //fin metodo getRequest()

	public void setRequest(HttpServletRequest req)
	{
		request = req;
	}

	public void setResponse(HttpServletResponse resp)
	{
		response = resp;
	}

	public void setSession(HttpSession ses){
		this.session = ses;
	}

	public String getSingleParamValue(String param)
		throws Exception
	{
		String valorParam=null;
		if (this.existsParamValue(param)){
			valorParam = request.getParameterValues(param)[0];
		}
		return valorParam;
	}

	public String[] getMultipleParamValues(String param)
		throws Exception
	{
		String[] valores=null;
		if (existsParamValue(param)){
			valores = request.getParameterValues(param);
		}
		return valores;
	}



	public boolean existsParamValue(String param)
	{
		if (request.getParameterValues(param)==null){
			return false;
		}
		else{
			return true;
		}
	}

	public Enumeration getParameterNames()
	{
		return request.getParameterNames();
	}

	public String getServletPath()
	{
		HttpServletRequest request = this.getRequest();
		return request.getServletPath();
	}

	//para usar cuando necesitamos el "full path" al servlet
	public String getRequestURL(){
		HttpServletRequest request = this.getRequest();
		StringBuffer url =HttpUtils.getRequestURL(request);
		return url.toString();
	}

	public boolean requestEstaVacio()
	{
		if (this.getRequest().getParameterNames().hasMoreElements()){
			return false;
		}
		else{
			return true;
		}
	}

	public void loginSession()
	{
		this.logoutSession();
		HttpSession sess=this.getRequest().getSession(true);
		this.setSession(sess);
	}


	public void removeAllDataFromSession()
	{
		HttpSession sess = this.getSession();
		String theValueNames[];
		theValueNames = (String[])sess.getValueNames();
		int numberOfValues = theValueNames.length;
		for (int i=0; i<numberOfValues; i++)
		{
			String name = theValueNames[i];
			sess.removeValue(name);
		}  // end of FOR
	}  // end of removeAllDataFromSession()


	public void logoutSession()
	{
		HttpSession sess = this.getSession();
		if (sess!=null)
		{
			this.removeAllDataFromSession();
			sess.invalidate();
		}
	}

	/**
	 * Deprecated. Should use putObjectInSession().
	 */
	public void putInSession(String name, Object obj)
	{
		this.putObjectInSession(name, obj);
	}

	public Object getObjectFromSession(String name) throws Exception
	{
		Object valor =  this.getSession().getValue(name);
		if (valor==null)
		{
			throw new Exception("El siguiente campo no existe en la Session: "+name);
		}
		return valor;
	}

	public void removeFromSession(String name)
	{
		this.getSession().removeValue(name);
	}


	public ServletOutputStream getServletOutputStream() throws IOException
	{
		this.getResponse().setContentType("text/html");
		return this.getResponse().getOutputStream();
	}


	//la session id de este usuario
	public String getSessionId()
	{
		return this.getSession().getId();
	}

	//todas las sessiones activas en el servidor
	//  devuelve un enumeration de strings
	public Enumeration getAllSessionIds()
	{
		return this.getSession().getSessionContext().getIds();
	} // END OF getAllSessionIds


	/**
	 * Verifica que exista un objeto en la sessi�n.
	 * @param   _sObjectName    Nombre del objeto.
	 * @return                  Verdarero si el objeto exite y
	 *                          falso caso contrario.
	 */
	public boolean existsObjectInSession(String _sObjectName) {
		boolean exists = true ;
		try{
			this.getFromSession(_sObjectName)   ;
		} catch (Exception NoExisteCampoEnSessionException){
			exists = false                      ;
		}
		return exists;
	} // END OF public boolean existsObjectInSession()


	//desde SnoopServlet para getRequestInfo()
	private void print(PrintWriter out, String name, String value)
		throws IOException
	{
		out.print(" " + name + ": ");
		out.println(value == null ? "&lt;none&gt;" : value);
	}//fin print(ServletOutputStream out, String name, String value)

	//desde SnoopServlet para getRequestInfo()
	private void print(PrintWriter out, String name, int value)
		throws IOException
	{
		out.print(" " + name + ": ");
		if (value == -1) {
		    out.println("&lt;none&gt;");
		} else {
		    out.println(value);
		}
	} //fin print(PrintWriter out, String name, int value)


	/**
	   Devuelte la cantidad de par�mtros que tiene el Request.
	   @return
	 */
	public int getCantidadParametros() {
		int numero = 0 ;
		java.util.Enumeration e = request.getParameterNames();
		while (e.hasMoreElements())
			numero++ ;
		return numero ;
	} // END OF public int getCantidadParametros()


} //fin class ConeccionHTTP